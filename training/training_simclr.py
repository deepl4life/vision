import pytorch_lightning as pl
from argparse import Namespace
from pytorch_lightning.loggers import TensorBoardLogger
from VISION.data.dataset.sample_pair_generation_dataset import ApeusPairSampler, SamplePairGeneration
from VISION.data.loaders.sample_pair_generation_loader import SimCLRDataModule
from VISION.models.self_supervised.contrastive_loss import ContrastiveLoss
from VISION.models.self_supervised.simclr.image_embedding import ImageEmbedding
from VISION.models.self_supervised.simclr.simclr import SimCLR
from VISION.models.self_supervised.simclr.transform import SimCLRDataTransform


def train(cfg: Namespace):
    # PAIR SAMPLING AND TRANSFORMATION
    pair_sampler = ApeusPairSampler(clip_duration=cfg.clip_duration, blending_factor=cfg.blending_factor)
    pair_transform = SimCLRDataTransform(resize_input=cfg.resize,
                                         gaussian_blur=cfg.gaussian_blur,
                                         jitter_strength=cfg.jitter_strenght,
                                         normalize=cfg.normalized)

    # TRAINING AND VALIDATION DATASET
    train_dataset = SamplePairGeneration(path_dir_video=cfg.path_directory_videos,
                                         path_json=cfg.path_json_train_video_names,
                                         positive_pair_sampler=pair_sampler,
                                         transform=pair_transform,
                                         extension_factor=10)

    val_dataset = SamplePairGeneration(path_dir_video=cfg.path_directory_videos,
                                       path_json=cfg.path_json_val_video_names,
                                       positive_pair_sampler=pair_sampler,
                                       transform=pair_transform,
                                       extension_factor=10)

    # LIGHTNING DATA MODULE
    datamodule = SimCLRDataModule(train_dataset=train_dataset,
                                  val_dataset=val_dataset,
                                  batch_size=cfg.batch_size,
                                  num_workers=cfg.num_workers,
                                  drop_last=True)

    # LOSS AND MODEL
    loss_function = ContrastiveLoss(batch_size=cfg.batch_size, temperature=cfg.temperature)
    image_embedder = ImageEmbedding(name_model=cfg.name_model,
                                    embedding_size=cfg.embedding_size,
                                    pretrained=cfg.pretrained)

    # LIGHTNING MODULE
    simclr_module = SimCLR(model=image_embedder,
                           loss=loss_function,
                           learning_rate=cfg.lr,
                           nb_steps=cfg.nb_steps)

    # CALLBACKS

    checkpoint_callback = pl.callbacks.ModelCheckpoint(dirpath=cfg.checkpoint_path,
                                                       monitor="VALIDATION LOSS",
                                                       save_top_k=cfg.save_top_k,
                                                       mode="min",
                                                       save_weights_only=True)

    logger = TensorBoardLogger(save_dir=cfg.tensorboard_save_dir,
                               name=cfg.name_logs,
                               default_hp_metric=False)

    trainer = pl.Trainer(weights_summary="full",
                         progress_bar_refresh_rate=1,
                         num_sanity_val_steps=0,
                         log_every_n_steps=2,
                         accelerator='ddp',
                         gpus=cfg.gpus,
                         max_steps=cfg.nb_steps,
                         callbacks=[checkpoint_callback],
                         logger=logger)

    trainer.fit(simclr_module, datamodule)


if __name__ == '__main__':
    hparams = Namespace(
        resize=(244, 224),
        gaussian_blur=True,
        jitter_strenght=0.3,
        normalized=False,
        clip_duration=10,
        blending_factor=0.3,
        path_directory_videos="/media/apeus/vol1/apeus_videos",
        path_json_train_video_names="/home/apeus/Desktop/dataset_v1/self_supervised_train_data.json",
        path_json_val_video_names="/home/apeus/Desktop/dataset_v1/self_supervised_val_data.json",
        batch_size=32,
        num_workers=0,
        temperature=0.5,
        name_model="resnet50",
        embedding_size=512,
        pretrained=False,
        lr=0.001,
        nb_steps=10000,
        gpus=4,
        checkpoint_path="/home/apeus/PycharmProjects/models/simclr",
        save_top_k=3,
        tensorboard_save_dir="/home/apeus/PycharmProjects/zeus/logs/ssl",
        name_logs="test"
    )
    train(hparams)
