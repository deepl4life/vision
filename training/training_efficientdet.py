import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from VISION.data.loaders import DetectEusDataModule, DetectDataModule
from VISION.data.dataset import ApeusDataset
from VISION.models.detection.efficientDet import EfficientDetBuilder, EfficientDet, FocalLoss
from VISION.models.detection.object_detector import ObjectDetector
from VISION.utils.utils import Params


# os.environ["CUDA_VISIBLE_DEVICES"] = "1, 2, 3"

def main(params: Params):
    datamodule = DetectEusDataModule(data_dir=params.data_dir,
                                     path_json_train=params.path_json_train,
                                     path_json_val=params.path_json_val,
                                     train_dataset=ApeusDataset,
                                     val_dataset=ApeusDataset,
                                     num_workers=params.num_workers,
                                     normalize=params.normalize,
                                     resize=params.resize,
                                     batch_size=params.batch_size,
                                     pin_memory=params.pin_memory,
                                     drop_last=params.drop_last)

    datamodule.setup()
    # input_sizes = [512, 640, 768, 896, 1024, 1280, 1280, 1536, 1536]
    model = EfficientDetBuilder(num_classes=2, compound_coef=2)
    loss = FocalLoss()
    model = EfficientDet(model, loss, weights_path="/home/afleurentin/Téléchargements/efficientdet-d2.pth")

    detector = ObjectDetector(datamodule=datamodule,
                              model=model,
                              learning_rate=params.learning_rate,
                              nb_steps=params.max_steps,
                              num_gpus=params.gpus)

    checkpoint_callback = pl.callbacks.ModelCheckpoint(dirpath=params.checkpoint_path,
                                                       monitor=params.monitor,
                                                       save_top_k=params.save_top_k,
                                                       mode=params.mode,
                                                       save_weights_only=True)

    lr_monitor = pl.callbacks.LearningRateMonitor(logging_interval='step')

    logger = TensorBoardLogger(save_dir="/home/afleurentin/PycharmProjects/zeus/logs/efficientDet",
                               name=params.fold,
                               default_hp_metric=False)

    trainer = pl.Trainer(weights_summary='full',
                         progress_bar_refresh_rate=10,
                         num_sanity_val_steps=0,
                         # accelerator='ddp',
                         gpus=1,
                         max_steps=params.max_steps,
                         auto_lr_find=params.auto_lr_find,
                         callbacks=[checkpoint_callback, lr_monitor],
                         logger=logger
                         )

    trainer.fit(detector, datamodule=datamodule)
    # print(f"[INFO]... Best model can be find at : {checkpoint_callback.best_model_path}")

    # print("[INFO]... Start inference")
    # predictions = trainer.predict(detector, datamodule=datamodule)
    # print(predictions)


if __name__ == "__main__":
    params = Params(config_file=r"D:\PycharmProjects\vision\configs\config-faster-rcnn-1GPU.yml")
    # params = Params("/home/afleurentin/PycharmProjects/zeus/configs/config-busi.yml")
    # params = Params(config_file="/home/apeus/PycharmProjects/zeus/MyTest/config/config-faster-rcnn_fold_0.yml")
    main(params)
    # params = Params(config_file="/home/apeus/PycharmProjects/zeus/MyTest/config/config-faster-rcnn_fold_1.yml")
    # main(params)
    # params = Params(config_file="/home/apeus/PycharmProjects/zeus/MyTest/config/config-faster-rcnn_fold_2.yml")
    # main(params)
    # params = Params(config_file="/home/apeus/PycharmProjects/zeus/MyTest/config/config-faster-rcnn_fold_3.yml")
    # main(params)
