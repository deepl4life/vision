import os
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from VISION.data.loaders.coco_eus_loader import DetectEusDataModule
from VISION.models.detection.retinanet import RetinaNet
from VISION.utils.utils import Params

os.environ["CUDA_VISIBLE_DEVICES"]="1, 2, 3"


def main(params: Params):
    datamodule = DetectEusDataModule(data_dir=params.data_dir,
                                     path_json_train=params.path_json_train,
                                     path_json_val=params.path_json_val,
                                     path_video=params.path_video,
                                     num_workers=params.num_workers,
                                     normalize=params.normalize,
                                     resize=params.resize,
                                     batch_size=params.batch_size,
                                     pin_memory=params.pin_memory,
                                     drop_last=params.drop_last)

    datamodule.setup()
    model = RetinaNet(val_loader=datamodule.val_dataloader(),
                      learning_rate=params.learning_rate,
                      backbone=params.backbone,
                      trainable_backbone_layers=5,
                      pretrained=params.pretrained,
                      nb_steps=params.max_steps,
                      num_gpus=params.gpus)

    checkpoint_callback = pl.callbacks.ModelCheckpoint(dirpath=params.checkpoint_path,
                                                       filename='{epoch}-retinanet',
                                                       monitor=params.monitor,
                                                       save_top_k=params.save_top_k,
                                                       mode=params.mode)

    lr_monitor = pl.callbacks.LearningRateMonitor(logging_interval='step')

    logger = TensorBoardLogger(save_dir="../MyTest/lightning_logs/trainable-layers", name=params.fold, default_hp_metric=False)

    trainer = pl.Trainer(weights_summary=None,
                         progress_bar_refresh_rate=100,
                         num_sanity_val_steps=100,
                         accelerator='ddp',
                         gpus=[0, 1, 2],
                         max_steps=params.max_steps,
                         auto_lr_find=params.auto_lr_find,
                         callbacks=[checkpoint_callback, lr_monitor],
                         logger=logger
                         )

    trainer.fit(model, datamodule=datamodule)
    print(f"[INFO]... Best model can be find at : {checkpoint_callback.best_model_path}")


if __name__ == "__main__":
    params = Params(config_file="/MyTest/config/config-faster-rcnn_fold_0.yml")
    main(params)
    params = Params(config_file="/MyTest/config/config-faster-rcnn_fold_1.yml")
    main(params)
    params = Params(config_file="/MyTest/config/config-faster-rcnn_fold_2.yml")
    main(params)
    params = Params(config_file="/MyTest/config/config-faster-rcnn_fold_3.yml")
    main(params)