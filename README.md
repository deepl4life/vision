# Vision

Small repository containing Pytorch and Pytorch lightning code for object detection

Models available:
----------------

>**OBJECTION DETECTION**
>* Faster RCNN (with and without Feature Pyramid Network)
>* RetinaNet
>* EfficientDet

>**SELF-SUPERVISED**
>* SimCLR 

_Multiple backbones available and pretrained model_

Customs dataset:
---------------

* Corporate report [download](https://drive.google.com/drive/folders/1HlxOuM_6TonFdGJkrSO-jEwKFg4h2C-F?usp=sharing) 
* BUSI (ultrasound images of breast cancer) [download](https://drive.google.com/drive/folders/1SnyhavczJWTbNuJp1iJEzu_ZEhW1viXZ?usp=sharing)

Usage
-----
* Create a new conda environment
* Clone the git repository
* Run `install_env.py` to install the necessary package
* Update the config file in `MyTest/configs`
* Use test file to start training
* open tensorboard to see the training progress


Coming soon:
------------

* CenterNet (object detection)
