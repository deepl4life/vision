import os

# create a new conda environement
# conda create -n dpl4life_vision python=3.7

list_packages = [
    "conda install --yes pytorch torchvision torchaudio cudatoolkit=10.2 -c pytorch",
    "conda install --yes -c conda-forge opencv",
    "conda install --yes -c conda-forge tensorboard",
    "conda install --yes -c conda-forge albumentations",
    "conda install --yes -c conda-forge pytorch-lightning",
    "pip install pycocotools-windows"
]

if os.name == 'nt':
    list_packages.append("pip install pycocotools-windows")
else:
    list_packages.append("conda install --yes -c conda-forge pycocotools")

for package in list_packages:
    os.system(package)