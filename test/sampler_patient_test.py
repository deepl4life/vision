import pytorch_lightning as pl
from VISION.data.dataset.predict_video_dataset import PredictVideoDataset
from VISION.data.dataset.coco_dataset import CocoDataset
from VISION.data.dataset.apeus_dataset import ApeusDataset
from VISION.data.loaders.coco_eus_loader import DetectDataModule, DetectEusDataModule
from VISION.utils.utils import Params
from VISION.utils.create_split_coco_json import split_coco_json
from VISION.models.detection.faster_rcnn import faster_rcnn
from VISION.models.detection.object_detector import ObjectDetector
from pytorch_lightning.loggers import TensorBoardLogger

path_data = "/DATA/APEUS/10V/images_crop"
path_train_anno = "/DATA/APEUS/10V/test_split/train_coco_crop.json"
path_val_anno = "/DATA/APEUS/10V/test_split/val_coco_crop.json"


split_coco_json(path_json="/DATA/APEUS/10V/coco_crop.json",
    val_split=[3, 36, 28],
    save_dir="/DATA/APEUS/10V/test_split")


params = Params(config_file="/home/ameyer/DEV/zeus/MyTest/config/config-test-samplerpatient.yml")
datamodule = DetectEusDataModule(data_dir=params.data_dir,
                              path_json_train=params.path_json_train,
                              path_json_val=params.path_json_val,
                              train_dataset=ApeusDataset,
                              val_dataset=ApeusDataset,
                              #predict_dataset=PredictVideoDataset,
                              num_workers=params.num_workers,
                              normalize=params.normalize,
                              resize=params.resize,
                              batch_size=params.batch_size,
                              pin_memory=params.pin_memory,
                              drop_last=params.drop_last,
                              subfolder=True)

datamodule.setup()
model = faster_rcnn(backbone=params.backbone,
                        fpn=True,
                        trainable_backbone_layers=5,
                        pretrained=params.pretrained,
                        num_classes=datamodule.num_classes)

detector = ObjectDetector(datamodule=datamodule,
                            model=model,
                            learning_rate=params.learning_rate,
                            nb_steps=params.max_steps,
                            num_gpus=params.gpus)

checkpoint_callback = pl.callbacks.ModelCheckpoint(dirpath=params.checkpoint_path,
                                                    monitor=params.monitor,
                                                    save_top_k=params.save_top_k,
                                                    mode=params.mode)

lr_monitor = pl.callbacks.LearningRateMonitor(logging_interval='step')

logger = TensorBoardLogger(save_dir="./logs", name=params.fold, default_hp_metric=False)

trainer = pl.Trainer(weights_summary='full',
                        progress_bar_refresh_rate=100,
                        num_sanity_val_steps=0,
                        #accelerator='ddp',
                        # gpus=[0, 1, 2],
                        gpus=params.gpus,
                        max_steps=params.max_steps,
                        auto_lr_find=params.auto_lr_find,
                        callbacks=[checkpoint_callback, lr_monitor],
                        logger=logger
                        )

trainer.fit(detector, datamodule=datamodule)

# print(len(train))
# it = iter(train)

# images, boxes, labels, = next(it)
# print(labels)
# print(boxes)
# print(len(images))
#print(images)
import matplotlib.pyplot as plt
# plt.imshow(images[0].permute(1, 2, 0))
# plt.show()