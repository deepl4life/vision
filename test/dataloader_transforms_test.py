from VISION.data.dataset.predict_video_dataset import PredictVideoDataset
from VISION.data.dataset.coco_dataset import CocoDataset
from VISION.data.loaders.coco_eus_loader import DetectDataModule
from VISION.utils.utils import Params

path_data = "/home/afleurentin/Bureau/dataset_v1/frames"
path_train_anno = "/home/afleurentin/Bureau/dataset_v1/train_dataset_v1_coco.json"
path_val_anno = "/home/afleurentin/Bureau/dataset_v1/val_dataset_v1_coco.json"


params = Params(config_file="/home/apeus/PycharmProjects/zeus/MyTest/config/config-busi.yml")
datamodule = DetectDataModule(data_dir=params.data_dir,
                              path_json_train=params.path_json_train,
                              path_json_val=params.path_json_val,
                              train_dataset=CocoDataset,
                              val_dataset=CocoDataset,
                              predict_dataset=PredictVideoDataset,
                              num_workers=params.num_workers,
                              normalize=params.normalize,
                              resize=params.resize,
                              batch_size=params.batch_size,
                              pin_memory=params.pin_memory,
                              drop_last=params.drop_last)

datamodule.setup()
train = datamodule.val_dataloader()
print(len(train))
it = iter(train)

images, boxes, labels, = next(it)
print(images)

