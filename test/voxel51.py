import fiftyone.zoo as foz

dataset = foz.load_zoo_dataset("quickstart")
print("DATASET")
print(dataset)

# Evaluate the objects in the `predictions` field with respect to the
# objects in the `ground_truth` field
results = dataset.evaluate_detections(
    "predictions",
    gt_field="ground_truth",
    eval_key="eval",
)

print("RESULT")