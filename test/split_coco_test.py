from VISION.utils.create_split_coco_json import split_coco_json
from pycocotools.coco import COCO

split_coco_json(path_json="/home/apeus/Desktop/dataset_v1/dataset_v1_coco.json",
                val_split=[30,38,36,49,3,39,6,51],
                save_dir="/home/apeus/Desktop/dataset_v1/annotations")

# coco = COCO("/home/afleurentin/Bureau/dataset_v1/dataset_v1_coco.json")
# print(coco.loadImgs())
# num_patients = [int(image_info['filename'].split('_')[0]) for image_info in coco.loadImgs(coco.getImgIds())]
# num_patients = len(set(num_patients))
# print(num_patients)