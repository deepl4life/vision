import json
path_json = "/home/afleurentin/Bureau/publaynet/instances_train.json"
with open(path_json) as json_file:
    data = json.load(json_file)

for k in range(len(data['images'])):
    data['images'][k]['filename'] = data['images'][k].pop('file_name')

for i in range(len(data['annotations'])):
    data['annotations'][i].pop('segmentation')

with open("/home/afleurentin/Bureau/publaynet/instances_train_trans.json", 'w') as json_file:
    json.dump(data, json_file)

