import numpy as np
import cv2
from VISION.utils.visualize import generate_bbox_colors, Visualize

image = np.zeros((500, 500, 3), np.uint8)
# image = Vizualize(image=image)


# Test draw_annotations and _draw_bbox :
# image.draw_image_annotations(boxes=[[20, 100, 400, 200], [80, 150, 200, 350]],
#                            class_names=['chat', 'chien'],
#                            scores=[0.9145, 0.789454])
# image.display()

# Test generate_bbox_colors
# colors = generate_bbox_colors(class_names=['chat', 'chien', 'rat', 'chien', 'rat'])
# print(colors)

# # Test draw_video_annotations
# path_video = "/home/afleurentin/Bureau/000034_VID004_cut_0.mp4"
# vid = cv2.VideoCapture(path_video)
# video = Vizualize(video=vid)
# number_frames = video.num_frames
# # generate bounding boxes
# bboxes = np.ones((number_frames, 2, 4))
# bboxes[:, 0] = [20, 100, 400, 200]
# bboxes[:, 1] = [80, 150, 200, 350]
# # generate labels
# labels = [['chat', 'chien'] for k in range(number_frames)]
# # colors
# dict_colors = {'chat': [255, 0, 0],
#                'chien': [0, 255, 0]}
# # draw annotations
# video.draw_video_annotations(path_new_video="/home/afleurentin/Bureau/000034_VID004_cut_1.mp4",
#                              boxes=bboxes,
#                              class_names=labels,
#                              dict_colors=dict_colors)

# Test mirror_predictions_ground_truth_apeus
path_video = "/home/afleurentin/Bureau/dataset_v1/videos/000009_VID001.mp4"
vid = cv2.VideoCapture(path_video)
video = Visualize(video=vid)
number_frames = video.num_frames
# generate bounding boxes
bboxes = np.ones((number_frames, 2, 4))
bboxes[:, 0] = [20, 100, 400, 200]
bboxes[:, 1] = [80, 150, 200, 350]
# generate labels
labels = [['chat', 'chien'] for k in range(number_frames)]
# colors
dict_colors = {'chat': [255, 0, 0],
               'chien': [0, 255, 0],
               'paren': [0, 0, 255],
               'lesion': [120, 120, 120]
               }

dict_correlation_label = {

    "paren_hete": "paren",
    "paren_hete_mild": "paren",
    "paren_hete_mod": "paren",
    "paren_hete_sev": "paren",

    "paren_homo_iso": "paren",

    "paren_homo_hypo": "paren",
    "paren_homo_hypo_mild": "paren",
    "paren_homo_hypo_mod": "paren",
    "paren_homo_hypo_sev": "paren",

    "paren_homo_hype": "paren",
    "paren_homo_hype_mild": "paren",
    "paren_homo_hype_mod": "paren",
    "paren_homo_hype_sev": "paren",

    "lesi_soli": "lesion",
    "lesi_soli_beni": "lesion",
    "lesi_soli_susp": "lesion",
    "lesi_soli_mali": "lesion",

    "lesi_cyst": "lesion",
    "lesi_cyst_beni": "lesion",
    "lesi_cyst_susp": "lesion",
    "lesi_cyst_mali": "lesion",
}

# draw annotations
video.mirror_predictions_ground_truth_apeus(path_new_video="/home/afleurentin/Bureau/000009_VID001_test.mp4",
                                            path_gt="/home/afleurentin/Bureau/dataset_v1/annotations/000009_VID001.json",
                                            correlation_label=dict_correlation_label,
                                            boxes=bboxes,
                                            class_names=labels,
                                            dict_colors=dict_colors,
                                            font_scale=1.5)