# import necessary packages
import torch
from typing import *
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone
from torchvision.models.detection.anchor_utils import AnchorGenerator
from torchvision.models.detection.retinanet import RetinaNetHead, RetinaNet as torchvision_RetinaNet
from VISION.models.backbones.torchvision_backbones import remove_frozen_batch_norm


def create_retinanet_backbone(backbone: str = "resnet50",
                              pretrained: bool = True,
                              trainable_backbone_layers: int = 5,
                              **kwargs: Any) -> torch.nn.Module:
    """ Creates a torchvision resnet model with fpn added.

    :param backbone: Supported backones are: "resnet18", "resnet34","resnet50", "resnet101", "resnet152",
            "resnext50_32x4d", "resnext101_32x8d", "wide_resnet50_2", "wide_resnet101_2", as resnets with fpn backbones.
    :param pretrained: If None or True creates imagenet weights backbone.
    :param trainable_backbone_layers: number of trainable resnet layers starting from final block.
    """

    backbone = resnet_fpn_backbone(backbone, pretrained=pretrained, trainable_layers=trainable_backbone_layers,
                                   **kwargs)
    backbone = remove_frozen_batch_norm(backbone)

    return backbone


def retinanet(backbone: str = None,
              pretrained: Union[bool, str] = True,
              trainable_backbone_layers: int = 5,
              num_classes: int = 2,
              **kwargs) -> torch.nn.Module:
    """
        :param backbone: Pretrained backbone CNN architecture.
        :param pretrained: if true, returns a model pre-trained on COCO train2017. If a path to a checkpoint is given,
        it will load the weights from this checkpoint.
        :param trainable_backbone_layers: Number of block to train in the backbone. if set at 5, it will train all
        the layers. if set to a lower value, the remaining layer will be freeze.
        :param num_classes: number of classes in the dataset.

    """
    # By default, RetinaNet has a background class, so we had to increment num_classes
    num_classes += 1
    checkpoint = None

    if isinstance(pretrained, str):
        checkpoint = pretrained
        pretrained = False

    backbone_model = create_retinanet_backbone(backbone=backbone,
                                               pretrained=pretrained,
                                               trainable_backbone_layers=trainable_backbone_layers,
                                               **kwargs)

    anchor_sizes = ((32,), (64,), (128,), (256,), (512,))
    aspect_ratios = ((0.5, 1.0, 2.0),) * len(anchor_sizes)
    anchor_generator = AnchorGenerator(sizes=anchor_sizes,
                                       aspect_ratios=aspect_ratios)

    model = torchvision_RetinaNet(backbone=backbone_model,
                                  num_classes=num_classes,
                                  anchor_generator=anchor_generator,
                                  image_mean=(0.559, 0.574, 0.566),
                                  image_std=(0.393, 0.395, 0.399),
                                  min_size=600,
                                  max_size=800,
                                  detections_per_img=100,
                                  fg_iou_thresh=0.4,
                                  bg_iou_thresh=0.2)

    if checkpoint:
        print("[INFO]... load weights from checkpoint")
        model.load_state_dict(torch.load(checkpoint)['state_dict'], strict=False)
        in_features = backbone_model.out_channels
        num_anchors = anchor_generator.num_anchors_per_location()[0]
        model.head = RetinaNetHead(in_features, num_anchors, num_classes)

    return model
