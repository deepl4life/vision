import torch
import torch.nn as nn
from torchvision.ops import box_iou
from typing import List, Dict


class FocalLoss(nn.Module):

    def __init__(self,
                 alpha: float = 0.25,
                 gamma: float = 2,
                 upper_threshold_objectness: float = 0.4,
                 lower_threshold_objectness: float = 0.3):

        super().__init__()
        self.alpha = alpha
        self.gamma = gamma
        self.upper_threshold_objectness = upper_threshold_objectness
        self.lower_threshold_objectness = lower_threshold_objectness

    @staticmethod
    def preprocess(targets: List[Dict[str, torch.Tensor]]):
        return [torch.cat([target["boxes"], target["labels"].unsqueeze(dim=1)], dim=1) for target in targets]

    def forward(self, classifications, regressions, anchors, annotations):
        """
        Compute the classification loss and the regression loss with the focal loss formula.
        The goal of the focal loss is to put the emphasize on the wrong classifications
        :param classifications: output of the classification layers (batch size, num anchors, num classes)
        :param regressions: output of the regression layers (batch_size, num anchors, 4)
        :param anchors (1, num anchors, 4)
        :param annotations: (batch size, num annotations, 5)
        :return: classification losses and regression losses
        """
        # parameters of the focal loss
        # these values are supposed to give the best result according to the paper
        alpha = self.alpha
        gamma = self.gamma
        batch_size = classifications.shape[0]
        classification_losses = []
        regression_losses = []

        anchor = anchors[0, :, :]  # anchors dim : (1, num_anchors, 4) -> anchor dim (num_anchors, 4)
        anchor_widths = anchor[:, 2] - anchor[:, 0]
        anchor_heights = anchor[:, 3] - anchor[:, 1]
        anchor_ctr_x = anchor[:, 0] + 0.5 * anchor_widths
        anchor_ctr_y = anchor[:, 1] + 0.5 * anchor_heights

        # preprocess because annotations inputs are originally a list of dictionaries
        annotations = self.preprocess(targets=annotations)

        # Loop over the number of entities of the batch:
        for j in range(batch_size):

            classification = classifications[j, :, :]
            regression = regressions[j, :, :]
            bbox_annotation = annotations[j]
            # This implementation of focal loss considers 0 as the first label possible, so we need to subtract 1 to
            # every label
            bbox_annotation[:, 4] = bbox_annotation[:, 4] - torch.ones((1, bbox_annotation.size(0))).to(
                torch.device('cuda'))

            classification = torch.clamp(classification, 1e-4, 1.0 - 1e-4)

            IoU = box_iou(anchor, bbox_annotation[:, :4])  # size :(num_anchors, num_annotations)

            IoU_max, IoU_argmax = torch.max(IoU, dim=1)  # size :(num_anchors,1)

            ###############################################################################
            #                    compute the loss for classification
            ###############################################################################
            # initialisation targets
            targets = torch.ones(classification.shape) * -1
            targets = targets.to(torch.device('cuda'))

            # set targets = 0 for every IoU_max < lower_threshold_objectness. The value proposed in the article is 0.4
            # Everything with a IoU score under lower_threshold_objectness will be considered as background
            targets[torch.lt(IoU_max, self.lower_threshold_objectness), :] = 0
            # get every index where IoU_max >= 0.2
            # Everything with a IoU score higher than upper_threshold_objectness will be considered as an object
            positive_indices = torch.ge(IoU_max, self.upper_threshold_objectness)  # The proposed in the article is 0.5

            num_positive_anchors = positive_indices.sum()

            # associate every anchors with the corresponding bbox (ground truth)
            assigned_annotations = bbox_annotation[IoU_argmax, :]  # size : (num anchors, 5)

            targets[positive_indices, :] = 0
            # set the class id for every anchors having a IoU >= 0.5
            targets[positive_indices, assigned_annotations[positive_indices, 4].long()] = 1

            # Recall formula for the classification loss:
            # L_cls_i = alpha*yi*log(pi)(1-pi)**gamma + (1-alpha)(1-yi)*log(1-pi)pi**gamma
            # - yi : equals 1 if the ground-truth belongs to the i-th class and 0 otherwise
            # - pi : is the predicted probability for the i-th class
            # L_cls = -sum(L_cls_i) for i=1 to num_classes

            # Compute the alpha and (1-alpha) from the L_cls_i formula
            alpha_factor = torch.ones(targets.shape).to(torch.device('cuda')) * alpha
            alpha_factor = torch.where(torch.eq(targets, 1.).to(torch.device('cuda')), alpha_factor, 1. - alpha_factor)
            # Compute the pi and (1-pi) from the L_cls_i formula
            focal_weight = torch.where(torch.eq(targets, 1.).to(torch.device('cuda')), 1. - classification,
                                       classification)
            # alpha*yi*(1-pi)**gamma + (1-alpha)(1-yi)*pi**gamma
            focal_weight = alpha_factor * torch.pow(focal_weight, gamma)
            # -log(pi) and -log(1-pi)

            bce = -(targets * torch.log(classification) + (1.0 - targets) * torch.log(1.0 - classification))

            cls_loss_i = focal_weight * bce

            # Recall (default value):
            # IoU score < 0.4 -> background
            # IoU score >= 0.5 -> object
            # Set cls_loss_i = 0 for IoU E [0.4,0.5], indeed we ignore them
            cls_loss_i = torch.where(torch.ne(targets, -1.0), cls_loss_i,
                                     torch.zeros(cls_loss_i.shape).to(torch.device('cuda')))

            # Sum the cls_loss_i and averaging
            classification_losses.append(cls_loss_i.sum() / torch.clamp(num_positive_anchors.float(), min=1.0))

            ###############################################################################
            #                    compute the loss for regression
            ###############################################################################

            if positive_indices.sum() > 0:
                # keep only the retained object
                assigned_annotations = assigned_annotations[positive_indices, :]

                anchor_widths_pi = anchor_widths[positive_indices]
                anchor_heights_pi = anchor_heights[positive_indices]
                anchor_ctr_x_pi = anchor_ctr_x[positive_indices]
                anchor_ctr_y_pi = anchor_ctr_y[positive_indices]

                # Ground truth are in (x1,y1,x2,y2) and we need (x_center, y_center, width, height)
                gt_widths = assigned_annotations[:, 2] - assigned_annotations[:, 0]
                gt_heights = assigned_annotations[:, 3] - assigned_annotations[:, 1]
                gt_ctr_x = assigned_annotations[:, 0] + 0.5 * gt_widths
                gt_ctr_y = assigned_annotations[:, 1] + 0.5 * gt_heights

                # clip widths and heights to 1
                gt_widths = torch.clamp(gt_widths, min=1)
                gt_heights = torch.clamp(gt_heights, min=1)

                # define regression targets (the standard parametrization used in object detection)
                targets_dx = (gt_ctr_x - anchor_ctr_x_pi) / anchor_widths_pi
                targets_dy = (gt_ctr_y - anchor_ctr_y_pi) / anchor_heights_pi
                targets_dw = torch.log(gt_widths / anchor_widths_pi)
                targets_dh = torch.log(gt_heights / anchor_heights_pi)

                targets = torch.stack((targets_dx, targets_dy, targets_dw, targets_dh))
                targets = targets.t()

                regression_diff = torch.abs(targets - regression[positive_indices, :])

                # smooth L1
                regression_loss = torch.where(
                    torch.le(regression_diff, 1.0 / 9.0),
                    0.5 * 9.0 * torch.pow(regression_diff, 2),
                    regression_diff - 0.5 / 9.0
                )
                regression_losses.append(regression_loss.mean())
            else:
                # if torch.cuda.is_available():
                #     regression_losses.append(torch.tensor(0).float().cuda())
                # else:
                regression_losses.append(torch.tensor(0).float())

        return torch.stack(classification_losses).mean(dim=0, keepdim=True), \
               torch.stack(regression_losses).mean(dim=0, keepdim=True)
