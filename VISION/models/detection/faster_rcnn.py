# import necessary packages
import torch
from typing import *
from torchvision.models.detection.backbone_utils import resnet_fpn_backbone
from torchvision.models.detection.anchor_utils import AnchorGenerator
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor, FasterRCNN as torchvision_FasterRCNN

from VISION.models.backbones.torchvision_backbones import create_torchvision_backbone, remove_frozen_batch_norm
from VISION.utils.utils import my_logger

logger = my_logger()


def create_fasterrcnn_backbone(backbone: str,
                               fpn: bool = True,
                               pretrained: bool = True,
                               trainable_backbone_layers: int = 5,
                               **kwargs: Any) -> torch.nn.Module:
    """
    :param backbone: Supported backones are: "resnet18", "resnet34","resnet50", "resnet101", "resnet152",
            "resnext50_32x4d", "resnext101_32x8d", "wide_resnet50_2", "wide_resnet101_2", as resnets with fpn backbones.
            Without fpn backbones supported are: "resnet18", "resnet34", "resnet50","resnet101",
            "resnet152", "resnext101_32x8d", "mobilenet_v2", "vgg11", "vgg13", "vgg16", "vgg19",
    :param fpn: If True then constructs fpn as well.
    :param pretrained: If None or True creates imagenet weights backbone.
    :param trainable_backbone_layers: number of trainable resnet layers starting from final block.
    """

    if fpn:
        # Creates a torchvision resnet model with fpn added.
        backbone = resnet_fpn_backbone(backbone, pretrained=True, trainable_layers=trainable_backbone_layers, **kwargs)

    else:
        # This does not create fpn backbone, it is supported for all models
        backbone, _ = create_torchvision_backbone(backbone, pretrained)

    backbone = remove_frozen_batch_norm(backbone)
    return backbone


def faster_rcnn(backbone: str = None,
                fpn: bool = True,
                pretrained: Union[bool, str] = True,
                trainable_backbone_layers: int = 5,
                num_classes: int = 2,
                min_size=600,
                max_size=800,
                rpn_pre_nms_top_n_train=500,
                rpn_pre_nms_top_n_test=500,
                rpn_post_nms_top_n_train=200,
                rpn_post_nms_top_n_test=200,
                **kwargs) -> torch.nn.Module:
    """
        :param backbone: Pretrained backbone CNN architecture.
        :param fpn: if True, it will add a Feature Pyramid Network (4 layers) on top of the backbone.
        :param pretrained: if true, returns a model pre-trained on COCO train2017. If a path to a checkpoint is given,
        it will load the weights from this checkpoint.
        :param trainable_backbone_layers: Number of block to train in the backbone. if set at 5, it will train all
        the layers. if set to a lower value, the remaining layer will be freeze.
        :param num_classes: number of classes in the dataset.

    """

    # By default, Faster RCNN has a background class, so we had to increment num_classes
    num_classes += 1
    checkpoint = None

    if isinstance(pretrained, str):
        checkpoint = pretrained
        pretrained = False

    backbone_model = create_fasterrcnn_backbone(backbone=backbone,
                                                fpn=fpn,
                                                pretrained=pretrained,
                                                trainable_backbone_layers=trainable_backbone_layers,
                                                **kwargs)

    if fpn:
        anchor_sizes = ((32,), (64,), (128,), (256,), (512,))
        aspect_ratios = ((0.5, 1.0, 2.0),) * len(anchor_sizes)
        anchor_generator = AnchorGenerator(sizes=anchor_sizes,
                                           aspect_ratios=aspect_ratios)

    else:
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))

    model = torchvision_FasterRCNN(backbone=backbone_model,
                                   rpn_anchor_generator=anchor_generator,
                                   num_classes=num_classes,
                                   image_mean=[0.559, 0.574, 0.566],
                                   image_std=[0.393, 0.395, 0.399],
                                   min_size=min_size,
                                   max_size=max_size,
                                   rpn_pre_nms_top_n_train=rpn_pre_nms_top_n_train,
                                   rpn_pre_nms_top_n_test=rpn_pre_nms_top_n_test,
                                   rpn_post_nms_top_n_train=rpn_post_nms_top_n_train,
                                   rpn_post_nms_top_n_test=rpn_post_nms_top_n_test)

    if checkpoint:
        logger.info("Loading weights from checkpoint")
        state_dict = torch.load(checkpoint)['state_dict']
        try:
            model.load_state_dict(state_dict, strict=True)
            return model
        except RuntimeError:
            logger.warning("Trying to change layers name to load state_dict")
            for key in list(state_dict.keys()):
                state_dict[key.replace('model.', '')] = state_dict.pop(key)

        try:
            model.load_state_dict(state_dict, strict=True)
        except RuntimeError:
            model.load_state_dict(state_dict, strict=False)
            in_features = model.roi_heads.box_predictor.cls_score.in_features
            model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
            logger.warning("the head of the model is changed")

    return model
