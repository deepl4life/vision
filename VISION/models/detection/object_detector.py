# import necessary packages
import torch
import pytorch_lightning as pl
import numpy as np
from typing import *
from VISION.models.detection import metrics
from VISION.data.loaders.coco_eus_loader import DetectEusDataModule

from VISION.utils.utils import convert_coco_bboxes_to_tensor


class ObjectDetector(pl.LightningModule):

    def __init__(self,
                 datamodule: pl.LightningDataModule,
                 model: torch.nn.Module,
                 learning_rate: float = 0.001,
                 nb_steps: int = 50000,
                 num_gpus: int = 1):
        """

        :param datamodule:
        :param model:
        :param learning_rate:
        :param nb_steps:
        :param num_gpus:
        """
        super().__init__()

        self.num_gpus = num_gpus
        self.is_distributed = True if self.num_gpus > 1 else False
        self.nb_steps = nb_steps
        self.datamodule = datamodule
        self.learning_rate = learning_rate
        self.num_classes = int(self.datamodule.num_classes) + 1  # num of classes + background
        self.model = model

        if isinstance(datamodule, DetectEusDataModule):
            self.num_patient_ids = self.datamodule.num_patients
            self.patient_ids = self.datamodule.patient_ids

        # self.save_hyperparameters()

    def forward(self, x):

        return self.model(x)

    def training_step(self, batch, batch_idx):

        if isinstance(self.datamodule, DetectEusDataModule):
            images, bboxes, labels, _ = batch
        else:
            images, bboxes, labels = batch

        # Transform what is returned by the dataloader into a correct input for the model
        images = list(images)
        targets = []
        # Loop over the images inside the batch
        for box, label in zip(bboxes, labels):

            if len(label) == 0:
                target_dict = {"boxes": torch.zeros((0, 4), dtype=torch.float32).to(torch.device('cuda')),
                               "labels": torch.zeros(1, dtype=torch.int64).to(torch.device('cuda'))}
            else:
                target_dict = {"boxes": convert_coco_bboxes_to_tensor(np.array(box)).to(torch.device('cuda')),
                               "labels": torch.from_numpy(np.array(label)).type(dtype=torch.int64).to(
                                   torch.device('cuda'))}
            targets.append(target_dict)

        # faster-rcnn takes both images and targets for training, returns
        loss_dict = self.model(images, targets)
        loss = sum(loss for loss in loss_dict.values())

        # log the different loss values in tensorboard
        for k, v in loss_dict.items():
            self.log("LOSSES/" + k, v, sync_dist=True)
        self.log("GLOBAL LOSS", loss, sync_dist=True)

        return loss

    def validation_step(self, batch, batch_idx):

        if isinstance(self.datamodule, DetectEusDataModule):
            metrics.apeus_validation_step(self, batch, batch_idx)
        else:
            metrics.coco_validation_step(self, batch, batch_idx)

    def validation_epoch_end(self, outputs: List[Any]) -> None:

        if isinstance(self.datamodule, DetectEusDataModule):
            metrics.apeus_validation_epoch_end(self)
        else:
            metrics.coco_validation_epoch_end(self)

    def predict_step(self, batch, batch_idx, dataloader_idx):
        images, crop_coordinates = batch
        predictions = self.model(images)

        return {"predictions": predictions, "crop_coordinates": crop_coordinates}

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))

        optimizer = torch.optim.SGD(params=trainable_parameters,
                                    lr=self.learning_rate,
                                    momentum=0.9,
                                    weight_decay=0.005)

        # optimizer = torch.optim.Adam(params=trainable_parameters,
        #                              lr=self.learning_rate,
        #                              betas=(0.9, 0.995),
        #                              weight_decay=0.005)

        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer, T_max=self.nb_steps)

        # lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer=optimizer,
        #                                                step_size=3,
        #                                                gamma=0.2)

        return [optimizer], [lr_scheduler]
