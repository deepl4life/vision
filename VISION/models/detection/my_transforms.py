from albumentations.core.transforms_interface import ImageOnlyTransform


class Rescale_0_1(ImageOnlyTransform):
    def __init__(self):
        super().__init__()
        self.always_apply = True

    def apply(self, img, **params):
        img2 = img
        rescale_img = (img - img.min()) / (img.max() - img.min())
        return rescale_img
