import numpy as np
from VISION.utils.coco_evaluator import CocoEvaluator, name_metrics
from VISION.utils.utils import synchronize_pycocotools_metrics


def coco_validation_step(self, batch, batch_idx):
    """ Function to use in method validation_step of a lightning module

        Tested with:
         - faster_rcnn
         - retinanet
    """
    images, bboxes, labels = batch

    predictions = self.model(images)
    # If it's the first batch, we create the COCOEvaluator first
    if batch_idx == 0:
        # Overall metric
        self.coco_evaluator = CocoEvaluator(self.datamodule)

    # Update overall metric
    self.coco_evaluator.update_gt(bboxes=bboxes, labels=labels)
    self.coco_evaluator.update_pred(predictions)


def coco_validation_epoch_end(self):
    # Compute overall metrics
    print("\n[INFO]... Global metric")
    metrics = self.coco_evaluator.compute_metrics()

    # gather all the metrics from the different processes
    metrics_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.stats),
                                                    self.is_distributed)

    metrics_category_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.category_stats),
                                                             self.is_distributed)
    # List of the labels (name)
    labels = list(self.datamodule.labels)

    # Log the metrics in Tensorboard
    for k, v in name_metrics.items():
        self.log("GLOBAL METRICS/" + v, metrics_stats[k])
        # For the mAP with IoU, we want the statistics per classes
        if k == 1:
            for i in range(self.num_classes - 1):
                self.log("GLOBAL METRICS/CATEGORY/" + labels[i] + " " + v, metrics_category_stats[k, i])


def apeus_validation_step(self, batch, batch_idx):
    """ Function to use in method validation_step of a lightning module.
    This function is designed to gather update the Coco Evaluator for the APEUS data

    Tested with:
     - faster_rcnn
     - retinanet
     """
    images, bboxes, labels, patient_ids = batch

    predictions = self.model(images)
    # If it's the first batch, we create the COCOEvaluator first
    if batch_idx == 0:
        # Overall metric
        self.coco_evaluator = CocoEvaluator(self.datamodule)
        # Metric per patient. Initialize a CocoEvaluator for each patient in a dictionary
        self.patient_coco_evaluator = {}
        for id_patient in self.patient_ids:
            self.patient_coco_evaluator[id_patient] = CocoEvaluator(self.datamodule)

    # Update overall metric
    self.coco_evaluator.update_gt(bboxes=bboxes, labels=labels)
    self.coco_evaluator.update_pred(predictions)

    # Update metric per patient
    for i, (bbox, label, patient_id) in enumerate(zip(bboxes, labels, patient_ids)):
        self.patient_coco_evaluator[patient_id[0]].update_gt(bboxes=[bbox], labels=[label])
        self.patient_coco_evaluator[patient_id[0]].update_pred([predictions[i]])


def apeus_validation_epoch_end(self):
    """ Function to use in method validation_epoch_end of a lightning module.
    This function is designed to compute and log the metrics for the APEUS data

    Tested with:
     - faster_rcnn
     - retinanet
    """
    # Compute overall metrics
    print("\n[INFO]... Global metric")
    metrics = self.coco_evaluator.compute_metrics()

    # gather all the metrics from the different processes
    metrics_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.stats),
                                                    self.is_distributed)

    metrics_category_stats = synchronize_pycocotools_metrics(self.all_gather(metrics.category_stats),
                                                             self.is_distributed)

    # List of the labels (name)
    labels = list(self.datamodule.labels)

    # Log the metrics in Tensorboard
    for k, v in name_metrics.items():
        self.log("GLOBAL METRICS/" + v, metrics_stats[k])
        # For the mAP with IoU, we want the statistics per classes
        if k == 1:
            for i in range(self.num_classes - 1):
                self.log("GLOBAL METRICS/CATEGORY/" + labels[i] + " " + v, metrics_category_stats[k, i])

    # Compute metrics per patient
    patient_metrics = {}
    patient_metrics_categories = {}
    global_metrics_patient_wise = []
    global_metrics_categories_patient_wise = []
    for id_patient in self.patient_ids:
        print(f"[INFO]... Evaluation patient : {id_patient}")
        patient_coco_result = self.patient_coco_evaluator[id_patient].compute_metrics()
        patient_metrics[id_patient] = synchronize_pycocotools_metrics(self.all_gather(patient_coco_result.stats),
                                                                      self.is_distributed)
        patient_metrics_categories[id_patient] = synchronize_pycocotools_metrics(
            self.all_gather(patient_coco_result.category_stats),
            self.is_distributed)
        # Keep the mAP IoU 50% for each patient
        global_metrics_patient_wise.append(patient_metrics[id_patient][1].cpu().numpy())
        # Keep the mAP IoU 50% for each categories of each patient
        global_metrics_categories_patient_wise.append(patient_metrics_categories[id_patient][1].cpu().numpy())

        # Log the metrics in Tensorboard
        for k, v in name_metrics.items():
            # For the mAP with IoU, we want the statistics per classes
            if k == 1:
                self.log(f"METRICS PER PATIENTS/Patient n°{id_patient}/" + v, patient_metrics[id_patient][k])
                for i in range(self.num_classes - 1):
                    self.log(f"METRICS PER PATIENTS/Patient n°{id_patient}/CATEGORY/" + labels[i] + " " + v,
                             patient_metrics_categories[id_patient][k, i])

    self.log("GLOBAL METRICS/Mean Average Precision Patient-wise [ IoU=0.50 | area= all | maxDets=100 ]",
             np.mean(global_metrics_patient_wise))

    global_metrics_categories_patient_wise = np.array(global_metrics_categories_patient_wise)
    # Index to remove the -1 values. Otherwise the average will not return a correct value
    index_to_keep_gmcpw = global_metrics_categories_patient_wise > -1
    for i in range(self.num_classes - 1):
        self.log("GLOBAL METRICS/CATEGORY/ " + labels[i] + " mAP Patient-wise[ IoU=0.50 | area= all | maxDets=100 ]",
                 np.mean(global_metrics_categories_patient_wise[index_to_keep_gmcpw[:, i], i], axis=0))
