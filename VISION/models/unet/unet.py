import sys
sys.path.append("/home/ameyer/DEV/zeus_perso/")

from more_itertools import windowed
import torch
from torch import nn
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import LearningRateMonitor
import monai
import pytorch_lightning as pl
from VISION.data.dataset.slowness_prior_dataset import VideoDataModule

class SlownessPriorLoss(nn.Module):
    """take a batch of encoded features and return the mean of the pairwise differences"""
    def __init__(self):
        super().__init__()

    def forward(self, encoded_input):
        # for i in range()
        loss = []
        for i in windowed(encoded_input, n=2, step=2):
            loss.append(abs(i[1] - i[0]))
        loss = torch.stack(loss)
        return torch.mean(loss)

class Block(nn.Module):
    def __init__(self, in_ch, out_ch, kernel_size=3):
        super().__init__()
        self.conv1 = nn.Conv2d(in_ch, out_ch, kernel_size, padding=1)
        self.relu = nn.ReLU()
        self.bn = nn.BatchNorm2d(out_ch)
        self.conv2 = nn.Conv2d(out_ch, out_ch, kernel_size, padding=1)
    
    def forward(self, x):
        return self.bn(self.relu(self.conv2(self.bn(self.relu(self.conv1(x))))))
    
class Encoder(nn.Module):
    def __init__(self, chs=(3, 32, 64, 128, 256, 512, 1024)):
        super().__init__()
        self.enc_blocks = nn.ModuleList([Block(chs[i], chs[i + 1]) for i in range(len(chs) - 1)])
        self.pool = nn.MaxPool2d(2)
    
    def forward(self, x):
        ftrs = []
        for block in self.enc_blocks:
            x = block(x)
            ftrs.append(x)
            x = self.pool(x)
        return ftrs
    
class Decoder(nn.Module):
    def __init__(self, chs=(1024, 512, 256, 128, 64, 32)):
        super().__init__()
        self.chs = chs
        self.upconvs = nn.ModuleList([nn.ConvTranspose2d(chs[i], chs[i + 1], 2, 2) for i in range(len(chs) - 1)])
        self.dec_blocks = nn.ModuleList([Block(chs[i + 1], chs[i + 1]) for i in range(len(chs) - 1)])
        
    def forward(self, x, encoder_features):
        for i in range(len(self.chs) - 1):
            x = self.upconvs[i](x)
            #x = torch.cat([x, encoder_features[i]], dim=1)
            x = self.dec_blocks[i](x)
        return x
    
class UNet(nn.Module):
    def __init__(self, enc_chs=(3, 32, 64, 128, 256, 512, 1024),
                 dec_chs=(1024, 512, 256, 128, 64, 32),
                 num_class=3):
        super().__init__()
        self.encoder = Encoder(enc_chs)
        self.decoder = Decoder(dec_chs)
        self.head = nn.Conv2d(dec_chs[-1], num_class, 1)
        self.sigmoid = nn.Sigmoid()
        
    def forward(self, x):
        enc_ftrs = self.encoder(x)
        out = self.decoder(enc_ftrs[::-1][0], enc_ftrs[::-1][1:])
        out = self.head(out)
        out = self.sigmoid(out)

        return out, enc_ftrs[-1]
    
class SlownessPriorModel(pl.LightningModule):

    def __init__(self,
                 net,
                 criterion=nn.MSELoss(),
                 learning_rate=1e-5,
                 optimizer_class=torch.optim.Adam):
        super().__init__()

        self.net = net
        self.lr = learning_rate
        self.criterion = criterion
        self.optimizer_class = optimizer_class
        
        self.example_input_array = torch.randn(4, 1, 512, 512)
        self.slownessloss = SlownessPriorLoss()
        
    def forward(self, x):
        return self.net(x)
    
    def configure_optimizers(self):
        optimizer = self.optimizer_class(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, 50000)
        return [optimizer], [scheduler]
    
    def prepare_batch(self, batch):
        return batch[0], batch[0]
    
    def infer_batch(self, batch):
        x, y = self.prepare_batch(batch)
        y_hat, enc_ftrs = self.net(x)
        return y_hat, enc_ftrs, y, x

    def training_step(self, batch, batch_idx):
        y_hat, enc_ftrs, y, x = self.infer_batch(batch)
        
        loss_reconstruction = self.criterion(y_hat, y)
        loss_slowness = self.slownessloss(enc_ftrs)
        final_loss = loss_reconstruction# + loss_slowness
        
        self.log('Loss/Train/loss_reconstruction', loss_reconstruction, on_step=True, on_epoch=False)
        self.log('Loss/Train/loss_slowness', loss_slowness, on_step=True, on_epoch=False)
        self.log('Loss/Train/final_loss', final_loss, on_step=True, on_epoch=False)
        
        index = self.current_epoch * 1000 + batch_idx
        
        if batch_idx % 10 == 0:
            monai.visualize.img2tensorboard.plot_2d_or_3d_image(
                x, index, self.logger.experiment,
                max_channels=1, tag="Train/ct")
            monai.visualize.img2tensorboard.plot_2d_or_3d_image(
                y_hat, index, self.logger.experiment,
                max_channels=1, tag="Train/pred")
                    
        return final_loss
    
    def validation_step(self, batch, batch_idx):
        y_hat, enc_ftrs, y, x = self.infer_batch(batch)
        
        loss_reconstruction = self.criterion(y_hat, y)
        loss_slowness = self.slownessloss(enc_ftrs)
        final_loss = loss_reconstruction #+ loss_slowness
        
        self.log('Loss/Evaluation/loss_reconstruction', loss_reconstruction, on_step=False, on_epoch=True)
        self.log('Loss/Evaluation/loss_slowness', loss_slowness, on_step=False, on_epoch=True)
        self.log('Loss/Evaluation/final_loss', final_loss, on_step=False, on_epoch=True)
        
        index = self.current_epoch * 1000 + batch_idx
        
        if batch_idx % 10 == 0:
            monai.visualize.img2tensorboard.plot_2d_or_3d_image(
                x, index, self.logger.experiment,
                max_channels=1, tag="Evaluation/ct")
            monai.visualize.img2tensorboard.plot_2d_or_3d_image(
                y_hat, index, self.logger.experiment,
                max_channels=1, tag="Evaluation/pred")

    
    def test_step(self, batch, batch_idx):
        self.logger.log_graph(self, input_array=self.example_input_array)
    
    
if __name__ == "__main__":
    
    print("\n***\tBlock\t\t***\n")
    
    enc_block = Block(3, 512)
    x = torch.randn(1, 3, 512, 512)
    print(enc_block(x).shape)
    
    print("\n***\tEncoder\t\t***\n")
    
    encoder = Encoder()
    ftrs = encoder(x)
    for ftr in ftrs:
        print(ftr.shape)
        
    print("\n***\tDecoder\t\t***\n")
    
    decoder = Decoder()
    x = torch.randn(1, 1024, 2, 2)
    print(decoder(x, ftrs[::-1][1:]).shape)
    
    print("\n***\tUNet\t\t***\n")
    
    unet = UNet()
    x = torch.randn(6, 3, 512, 512)
    out = unet(x)


    
    
    data = VideoDataModule()
    
    net=UNet(enc_chs=(1, 32, 64, 128),
                 dec_chs=(128, 64, 32),
                 num_class=1)
    
    model = SlownessPriorModel(
        net=net,
        learning_rate=1e-4,
        optimizer_class=torch.optim.Adam,
    )
    early_stopping = pl.callbacks.early_stopping.EarlyStopping(
        monitor='Loss/Evaluation/final_loss',
    )
    lr_monitor = LearningRateMonitor(logging_interval='epoch')
    logger = TensorBoardLogger("tb_logs", name="unet3D", log_graph=True, default_hp_metric=False)
    trainer = pl.Trainer(
        gpus=1,
        precision=32,
        callbacks=[
            #early_stopping,
            lr_monitor],
        logger=logger,
        max_steps=50000,
        max_epochs=10,
        profiler="simple",
        num_sanity_val_steps=0
    )
    trainer.fit(model=model, datamodule=data)