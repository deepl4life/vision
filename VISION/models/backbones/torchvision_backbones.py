import copy
import torch
import torch.nn as nn
import torchvision
from typing import Optional, Tuple

TORCHVISION_MODEL_ZOO = {
    "vgg11": torchvision.models.vgg11,
    "vgg13": torchvision.models.vgg13,
    "vgg16": torchvision.models.vgg16,
    "vgg19": torchvision.models.vgg19,
    "resnet18": torchvision.models.resnet18,
    "resnet34": torchvision.models.resnet34,
    "resnet50": torchvision.models.resnet50,
    "resnet101": torchvision.models.resnet101,
    "resnet152": torchvision.models.resnet152,
    "resnext50_32x4d": torchvision.models.resnext50_32x4d,
    "resnext50_32x8d": torchvision.models.resnext101_32x8d,
    "mnasnet0_5": torchvision.models.mnasnet0_5,
    "mnasnet0_75": torchvision.models.mnasnet0_75,
    "mnasnet1_0": torchvision.models.mnasnet1_0,
    "mnasnet1_3": torchvision.models.mnasnet1_3,
    "mobilenet_v2": torchvision.models.mobilenet_v2,
}


def create_backbone_generic(model: nn.Module, out_channels: int) -> nn.Module:
    """ Generic backbone creater. It removes the last linear layer.

    :param model: torch.nn.Module
    :param out_channels: numbers of output channels in the last layer
    :return: a pytorch module
    """
    layers_total = list(model.children())
    layers = layers_total[:-1]
    ft_backbone = nn.Sequential(*layers)
    ft_backbone.out_channels = out_channels

    return ft_backbone


# Use this when you have Adaptive Pooling layer in End.
# When Model.features is not applicable.
def create_backbone_adaptive(model: nn.Module, out_channels: Optional[int] = None) -> nn.Module:
    """ Creates backbone by removing linear and Adaptive Pooling layer.

    :param model: torch.nn model with adaptive pooling layer
    :param out_channels: numbers of output channels in the last layer
    :return: a pytorch module
    """

    if out_channels is None:
        modules_total = list(model.children())
        out_channels = modules_total[-1].in_features

    layers_total = list(model.children())
    layers = layers_total[:-1]
    model = nn.Sequential(*layers)

    return create_backbone_generic(model, out_channels=out_channels)


def create_backbone_features(model: nn.Module, out_channels: int) -> nn.Module:
    """ Creates backbone from feature sequential block.

    :param model: torch.nn model with features as sequential block.
    :param out_channels: Number of out_channels in last layer.
    :return: a pytorch module
    """
    ft_backbone = model.features
    ft_backbone.out_channels = out_channels

    return ft_backbone


def create_torchvision_backbone(model_name: str, pretrained: bool = True) -> Tuple[nn.Module, int]:
    """
    Creates CNN backbone from Torchvision.

    :param model_name: Name of the model
    :param pretrained: if true, it will load the weights pretrained on ImageNet
    """

    model_selected = TORCHVISION_MODEL_ZOO[model_name]
    net = model_selected(pretrained=pretrained)

    if model_name == "mobilenet_v2":
        out_channels = 1280
        ft_backbone = create_backbone_features(net, 1280)
        return ft_backbone, out_channels

    elif model_name in ["vgg11", "vgg13", "vgg16", "vgg19"]:
        out_channels = 512
        ft_backbone = create_backbone_features(net, out_channels)
        return ft_backbone, out_channels

    elif model_name in ["resnet18", "resnet34"]:
        out_channels = 512
        ft_backbone = create_backbone_adaptive(net, out_channels)
        return ft_backbone, out_channels

    elif model_name in [
        "resnet50",
        "resnet101",
        "resnet152",
        "resnext50_32x4d",
        "resnext101_32x8d",
    ]:
        out_channels = 2048
        ft_backbone = create_backbone_adaptive(net, out_channels)
        return ft_backbone, out_channels

    elif model_name in ["mnasnet0_5", "mnasnet0_75", "mnasnet1_0", "mnasnet1_3"]:
        out_channels = 1280
        ft_backbone = create_backbone_adaptive(net, out_channels)
        return ft_backbone, out_channels

    else:
        raise ValueError(f"Unsupported model: '{model_name}'")


def remove_frozen_batch_norm(backbone: nn.Module) -> nn.Module:
    """ When we create a backbone with resnet_fpn_backbone function, the layer of batch normalisation will be frozen. In
    order to do a proper fine-tunning training we need to switch them by BatchNorm layer.

    :param backbone: a resnet backbone
    :return: the resnet backbone with BatchNorm layer instead of FrozenBatchNorm layer
    """
    # extract the resnet backbone in our model
    try:
        resnet = backbone.body
    except torch.nn.modules.module.ModuleAttributeError:
        return backbone
    # Check for all FrozenBatchNorm layers
    frozenbn_to_replace = []
    for name, module in resnet.named_modules():
        if isinstance(module, torchvision.ops.misc.FrozenBatchNorm2d):
            frozenbn_to_replace.append(name)

    # # Iterate all layers to change
    for layer_name in frozenbn_to_replace:
        # Check if the layer is nested
        *parent, child = layer_name.split('.')

        # If the layer is nested
        if len(parent) > 0:
            # Get parent modules
            module = resnet.__getattr__(parent[0])
            for p in parent[1:]:
                module = module.__getattr__(p)
            # Get the FrozenBatchNorm2d layer
            frozen_bn = module.__getattr__(child)

        else:
            module = resnet.__getattr__(child)
            frozen_bn = copy.deepcopy(module)  # apparently we need that, otherwise you'll get an infinite recursion

        # Add our new layer here
        in_channels = frozen_bn.weight.shape[0]
        bn = torch.nn.BatchNorm2d(in_channels)
        with torch.no_grad():
            bn.weight = torch.nn.Parameter(frozen_bn.weight)
            bn.bias = torch.nn.Parameter(frozen_bn.bias)
            bn.running_mean = frozen_bn.running_mean
            bn.running_var = frozen_bn.running_var
        module.__setattr__(child, bn)

    # Fix the bn1 module
    resnet.bn1 = resnet.bn1.bn1

    return backbone