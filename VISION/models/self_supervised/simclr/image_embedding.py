import torch.nn as nn
from VISION.models.backbones.torchvision_backbones import create_torchvision_backbone, remove_frozen_batch_norm


class ImageEmbedding(nn.Module):

    def __init__(self, name_model: str = "resnet50", embedding_size: int = 512, pretrained: bool = False):
        super().__init__()
        base_model, out_channels = create_torchvision_backbone(name_model, pretrained)
        self.feature_extractor = remove_frozen_batch_norm(base_model)

        self.embedding = nn.Sequential(
            nn.AdaptiveAvgPool2d(output_size=(1, 1)),
            nn.Flatten(),
            nn.Identity()
        )
        self.projection = nn.Sequential(
            nn.Linear(in_features=out_channels, out_features=embedding_size),
            nn.ReLU(inplace=True),
            nn.Linear(in_features=embedding_size, out_features=embedding_size)
        )

    def forward(self, x):
        features = self.feature_extractor(x)
        embedding = self.embedding(features)
        projection = self.projection(embedding)
        return projection
