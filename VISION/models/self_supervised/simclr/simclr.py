import torch
from pytorch_lightning import LightningModule

from VISION.models.self_supervised.optimizer_lars import LARS


class SimCLR(LightningModule):

    def __init__(self,
                 model: torch.nn.Module,
                 loss: torch.nn.Module,
                 learning_rate: float = 0.001,
                 nb_steps: int = 1000):
        super().__init__()
        self.model = model
        self.loss = loss
        self.learning_rate = learning_rate
        self.nb_steps = nb_steps

    def forward(self, x):
        return self.model(x)

    def step(self, batch):
        x1, x2 = batch
        projection1, projection2 = self.forward(torch.stack(x1)), self.forward(torch.stack(x2))
        loss = self.loss(projection1, projection2)

        return loss

    def training_step(self, batch, batch_idx):
        loss = self.step(batch)
        self.log("TRAINING LOSS", loss, sync_dist=True, logger=True)
        return loss

    def validation_step(self, batch, batch_idx):
        loss = self.step(batch)
        self.log("VALIDATION LOSS", loss, sync_dist=True, logger=True)
        return loss

    def configure_optimizers(self):
        parameters = list(self.parameters())
        trainable_parameters = list(filter(lambda p: p.requires_grad, parameters))

        optimizer = LARS(trainable_parameters,
                         lr=self.learning_rate,
                         momentum=0.9,
                         weight_decay=1e-6,
                         trust_coefficient=0.001,)
        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer=optimizer, T_max=self.nb_steps)

        return [optimizer], [lr_scheduler]
