import string
import unicodedata


def unicode_to_ascii(s: str, all_letters: str = None) -> str:
    """ Transliterate any unicode string into the closest possible representation in ascii text
    code find here: https://stackoverflow.com/a/518232/2809427

    :param s: the unicode string to transform
    :param all_letters: string containing all the authorized characters
    :return: the ascii string
    """
    if all_letters is None:
        all_letters = string.ascii_letters + " :;.-0123456789"

    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn' and c in all_letters)
