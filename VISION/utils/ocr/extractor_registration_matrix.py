import cv2
import easyocr
import pandas as pd
import numpy as np
from tqdm import tqdm
from scipy.stats import zscore
from typing import List, Tuple
from VISION.utils.utils import my_logger

logger = my_logger()


def find_index_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx


class RecognitionMatrixExtractor:
    """ Extract the values of the transform matrix from a video of EUS-GPS software.

    Example:

    .. code-block:: python
        path_eus_video = "/path/to/my/eus_video.mp4"
        path_gps_video = "/path/to/my/gps_video.mp4"
        my_extractor= RecognitionMatrixExtractor(path_gps_video, path_eus_video)
        my_extractor.extraction()
        dataframe_transformation_matrix_values = my_extractor.check_data()

    .. warning:: the homogeneous coordinates are not extracted.

    .. warning:: some value are hard coded

    """

    def __init__(self,
                 path_gps_video: str,
                 path_eus_video: str = None):
        """
        :param path_gps_video: path to the video where the sensor value are displayed
        :param path_eus_video: path to the eus video. It's essentially to determine the number of frames to process
        """

        self.gps_video = cv2.VideoCapture(path_gps_video)
        self.eus_video = cv2.VideoCapture(path_eus_video) if path_eus_video else None
        self.df_registration_matrix = None
        self.num_frames = self._get_number_frames_to_process()
        self.data = {'R11': [], 'R12': [], 'R13': [], 'T1': [],
                     'R21': [], 'R22': [], 'R23': [], 'T2': [],
                     'R31': [], 'R32': [], 'R33': [], 'T3': []}

    def _get_number_frames_to_process(self):
        """ Return the number of frames to process. If the length of the eus_video is different from the length of the
        gps video, the number of frames will correspond to the minimum number of frames between the two videos """
        n_frames_gps = self.gps_video.get(cv2.CAP_PROP_FRAME_COUNT)

        if self.eus_video:
            n_frames_eus = self.eus_video.get(cv2.CAP_PROP_FRAME_COUNT)
            num_frames = min(n_frames_eus, n_frames_gps)
            if n_frames_gps != n_frames_eus:
                logger.info(f"EUS video has {n_frames_eus} and GPS video has {n_frames_gps}, "
                            f"so only the first {num_frames} will be processed")
        else:
            num_frames = n_frames_gps
            logger.info(f"{num_frames} frames will be processed")
        return int(num_frames)

    @staticmethod
    def disambiguate(ocr_value: str) -> str:
        """ try to disambiguate a string to be able to convert it into a float """
        if "~-" in ocr_value:
            ocr_value = ocr_value.replace("~-", "-")
        elif "~" in ocr_value:
            ocr_value = ocr_value.replace("~", "-")
        elif "," in ocr_value:
            ocr_value = ocr_value.replace(",", ".")
        elif ocr_value[-1] == ".":
            ocr_value = ocr_value[:-1]

        return ocr_value

    def _fill_index_data(self, index: int, extract_value: str):
        """ fill the data dictionary with a new value """
        key = list(self.data.keys())[index]
        try:
            self.data[key].append(float(extract_value))
        except ValueError:
            # try to change some character to make it works
            try:
                extract_value = self.disambiguate(extract_value)
                self.data[key].append(float(extract_value))
            except ValueError:
                print(f"wrong extracted_value: {extract_value}")
                self.data[key].append(np.NaN)

    def _fill_data(self, list_ocr_values: list):
        """ update the data dictionary data with ocr values extracted """
        if len(list_ocr_values) != 12:
            list_ocr_values = ["error"] * 12
        for i, value in enumerate(list_ocr_values):
            self._fill_index_data(i, value)

    @staticmethod
    def _preprocess_frame(frame: cv2.IMREAD_COLOR,
                          slice_crop_height: slice = slice(1030, 1080),
                          slice_crop_width: slice = slice(80, 1450)) -> cv2.THRESH_BINARY:
        """ Images preprocessing to feed to the OCR algorithm """
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # crop all the image except the part with the value
        frame = frame[slice_crop_height, slice_crop_width]
        h, w = frame.shape[:2]
        frame = cv2.resize(frame, (int(w * 3), int(h * 3)), cv2.INTER_CUBIC)
        # Binarisation and dilatation to highlight the digits
        _, frame = cv2.threshold(frame, 160, 255, cv2.THRESH_BINARY)
        kernel = np.ones((2, 2), np.uint8)
        frame = cv2.dilate(frame, kernel, iterations=2)

        return frame

    @staticmethod
    def _find_minus_signs(frame: cv2.THRESH_BINARY,
                          index_line_convolve: int = 71,
                          threshold_minus_signs: int = 3000) -> np.array:
        """ find the localisation of the minus signs in a frame """
        # convolve a specific line to find a approximate position of the minus signs
        result_conv = np.convolve(frame[index_line_convolve, :], np.repeat(1, 13))
        # keep only the higher convolutional value
        localisation_minus_signs = np.where(result_conv >= threshold_minus_signs)
        # After filtering, some positions can be redundant, so we remove them
        process_localisation_minus_signs = np.delete(localisation_minus_signs,
                                                     np.argwhere(np.ediff1d(localisation_minus_signs) <= 2) + 1)

        return process_localisation_minus_signs

    @staticmethod
    def _remove_minus_signs(frame: cv2.THRESH_BINARY, localisation_minus_signs: np.array) -> cv2.THRESH_BINARY:
        """ Remove the minus signs in a frame to ease the recognition of the digits """
        for loc_minus in localisation_minus_signs:
            frame[67:77, loc_minus - 20:loc_minus + 5] = 0

        return frame

    @staticmethod
    def _post_process_ocr_result(ocr_result: List[Tuple], loc_minus_signs: np.array) -> List:
        # sort result by their position in the frame
        ocr_result.sort(key=lambda data_value: data_value[0][0])
        # keep only digit extracted
        loc_ocr_value = [data[0][0][0] for data in ocr_result]

        # Put back the minus signs that we previously remove
        index_minus_sign = [find_index_nearest(loc_ocr_value, loc_minus) for loc_minus in loc_minus_signs]
        matrix_value = []  # list containing the final results
        for i, data in enumerate(ocr_result):
            prov_value = data[1]
            if i in index_minus_sign:
                value = '-' + prov_value
                matrix_value.append(value)
            else:
                matrix_value.append(prov_value)

        return matrix_value

    def extraction(self) -> pd.DataFrame:
        pbar = tqdm(total=self.num_frames)
        # Load the EasyOCR model
        reader = easyocr.Reader(['en'])

        for i in range(self.num_frames):
            _, frame = self.gps_video.read()
            frame = self._preprocess_frame(frame)
            loc_minus_signs = self._find_minus_signs(frame)
            frame = self._remove_minus_signs(frame, loc_minus_signs)

            ocr_value = reader.readtext(frame,
                                        detail=1,
                                        batch_size=12, allowlist=".0123456789")
            matrix_value = self._post_process_ocr_result(ocr_value, loc_minus_signs)
            self._fill_data(matrix_value)

            pbar.update()

        self.df_registration_matrix = pd.DataFrame(data=self.data)

        return self.df_registration_matrix

    def check_data(self, threshold_zscore_outliers: int = 10) -> pd.DataFrame:
        """ Remove the NaN value and the outliers in the data extracted """
        # Remove the "NaN" value by interpolate the value
        self.df_registration_matrix.interpolate()

        # Remove outliers
        z_scores = zscore(self.df_registration_matrix)
        abs_z_scores = np.abs(z_scores)
        filtered_entries = (abs_z_scores < threshold_zscore_outliers).all(axis=1)
        # Replace the row with a outlier with the previous row
        for row_index in np.where(filtered_entries == False):
            self.df_registration_matrix.loc[row_index] = self.df_registration_matrix.loc[row_index - 1]

        return self.df_registration_matrix
