import torch
import cv2
import torchvision
import torchvision.transforms.functional as F
from VISION.models.detection.faster_rcnn import faster_rcnn
from VISION.utils.visualize import Visualize




# resize box to target image size
def resize_boxes(boxes, original_size, new_size):
    ratios = [ns/os for ns, os in zip(new_size, original_size)]
    ratio_height, ratio_width = ratios
    xmin, ymin, xmax, ymax = boxes
    
    xmin = xmin * ratio_width
    xmax = xmax * ratio_width
    ymin = ymin * ratio_height
    ymax = ymax * ratio_height
    return torch.tensor([xmin, ymin, xmax, ymax])

def permute_and_flatten(layer, N, A, C, H, W):
    # type: (Tensor, int, int, int, int, int) -> Tensor
    layer = layer.view(N, -1, C, H, W)
    layer = layer.permute(0, 3, 4, 1, 2)
    layer = layer.reshape(N, -1, C)
    return layer

def concat_box_prediction_layers(box_cls, box_regression):
    # type: (List[Tensor], List[Tensor]) -> Tuple[Tensor, Tensor]
    box_cls_flattened = []
    box_regression_flattened = []
    # for each feature level, permute the outputs to make them be in the
    # same format as the labels. Note that the labels are computed for
    # all feature levels concatenated, so we keep the same representation
    # for the objectness and the box_regression
    for box_cls_per_level, box_regression_per_level in zip(
        box_cls, box_regression
    ):
        N, AxC, H, W = box_cls_per_level.shape
        Ax4 = box_regression_per_level.shape[1]
        A = Ax4 // 4
        C = AxC // A
        box_cls_per_level = permute_and_flatten(
            box_cls_per_level, N, A, C, H, W
        )
        box_cls_flattened.append(box_cls_per_level)

        box_regression_per_level = permute_and_flatten(
            box_regression_per_level, N, A, 4, H, W
        )
        box_regression_flattened.append(box_regression_per_level)
    # concatenate on the first dimension (representing the feature levels), to
    # take into account the way the labels were generated (with all feature maps
    # being concatenated as well)
    box_cls = torch.cat(box_cls_flattened, dim=1).flatten(0, -2)
    box_regression = torch.cat(box_regression_flattened, dim=1).reshape(-1, 4)
    return box_cls, box_regression

p_in = []
p_out = []

# Define hook_fn
def hook_fn(module, inputs, outputs):
    p_in.append(inputs)
    p_out.append(outputs)

def get_RPN_prediction(model, model_input,
                       size_resized_input_img=(800, 800), size_input_img=(1800, 1200),
                       score_threshold=0.5,
                       nms_overlaps_threshold=0.3):

    # place an hook on the rpn to capture i/o
    model.rpn.register_forward_hook(hook_fn)
    
    # the hook gave us a list of all input and all output
    # they are themself stocked in a list
    # in our case, we have 3 input and one output

    # p_in[0][0] --> ImageList: image
    # p_in[0][1] --> OrderedDict: features
    # p_in[0][1] --> targets (None in inf)

    # p_out[0][0][0] --> tensor for batch 0: 1000 best boxes 
    # p_out[0][1] --> Dict: loss (empty in inf)
    
    # predict with model
    outputs = model(model_input)

    images = p_in[-1][0]
    features = p_in[-1][1]

    features = list(p_in[-1][1].values())
    objectness, pred_bbox_deltas = model.rpn.head(features)
    anchors = model.rpn.anchor_generator(images, features)


    num_images = len(anchors)
    num_anchors_per_level_shape_tensors = [o[0].shape for o in objectness]
    num_anchors_per_level = [s[0] * s[1] * s[2] for s in num_anchors_per_level_shape_tensors]

    objectness, pred_bbox_deltas = concat_box_prediction_layers(objectness, pred_bbox_deltas)

    # apply pred_bbox_deltas to anchors to obtain the decoded proposals
    proposals = model.rpn.box_coder.decode(pred_bbox_deltas.detach(), anchors)
    proposals = proposals.view(num_images, -1, 4)
    boxes, scores = model.rpn.filter_proposals(proposals, objectness, images.image_sizes, num_anchors_per_level)
    
    # convert to proba
    scores[0] = torch.sigmoid(scores[0])

    bbox_pos = torch.stack([bbox for (bbox, score) in zip(boxes[0], scores[0]) if score > score_threshold])
    score_pos = torch.stack([score for score in scores[0] if score > score_threshold])

    resized_bbox = torch.stack([resize_boxes(bbox, size_resized_input_img, size_input_img)
                                      for bbox in bbox_pos])
    index_nms = torchvision.ops.nms(resized_bbox, score_pos, nms_overlaps_threshold)

    predictions = torch.index_select(resized_bbox, 0, index_nms)
    score_pos = torch.index_select(score_pos, 0, index_nms)

    return predictions, score_pos

def main():
    model = faster_rcnn(backbone='resnet50',
                        pretrained="/home/afleurentin/Téléchargements/epoch=2-step=10223-v1.ckpt",
                        min_size=800,
                        max_size=800,
                        rpn_pre_nms_top_n_test=2000,
                        rpn_post_nms_top_n_test=2000)
    model.eval()
    for k in range(20):
        image = cv2.imread(f"/home/afleurentin/Bureau/dataset_v1/frames/000009_VID001_{k*20}.png")
        h, w = image.shape[:2]
        input = F.to_tensor(image)

        results = get_RPN_prediction(model=model,
                                     model_input=[input],
                                     size_resized_input_img=(800, 800),
                                     size_input_img=(h, w),
                                     score_threshold=0.2)

        boxes, scores = results
        print(boxes)

        vis_image = Visualize(image)
        labels = ['object'] * len(boxes)
        vis_image.draw_image_annotations(boxes=boxes,
                                         class_names=labels,
                                         scores=scores,)
        vis_image.display()



if __name__ == "__main__":
    main()

