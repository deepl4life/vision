import json
from pycocotools.coco import COCO


def split_coco_json(path_json: str = None, val_split: list = None, save_dir: str = None) -> None:
    """ Split a COCO annotation file into a training and validation file according to specific patient ids

    :param str path_json: path to the annotation file (COCO format)
    :param list val_split: list of patient ids for the validation
    :param str save_dir: path to the directory where the new annotation files will be saved

    Example:

    .. code-block:: python

        split_coco_json(path_json="/home/afleurentin/Bureau/dataset_v1/coco_dataset_v1_copy.json",
        val_split=[33, 57, 18, 22, 16, 28, 24, 53],
        save_dir="/home/afleurentin/Bureau/dataset_v1")

    """

    # open the json with all the data and create two copies
    with open(path_json) as json_file:
        data = json.load(json_file)

    train_data = {'images': [], 'annotations': [], 'categories': []}
    val_data = {'images': [], 'annotations': [], 'categories': []}

    # Create a COCO object to facilitate the search of images and annotations
    coco = COCO(annotation_file=path_json)

    num_images = len(coco.getImgIds())
    num_anno = len(coco.getAnnIds())
    num_cate = len(coco.getCatIds())

    for k in range(num_cate):
        train_data["categories"].append(data["categories"][k])
        val_data["categories"].append(data["categories"][k])

    val_imgs_id = []
    # loop over the images to separate the train and validation images
    for j in range(num_images):
        name_img = data["images"][j]["filename"]
        id_img = data["images"][j]["id"]
        patient_id = int(name_img.split('_')[0])

        if patient_id in val_split:
            val_imgs_id.append(id_img)
            val_data["images"].append(data["images"][j])
        else:
            train_data["images"].append(data["images"][j])

    val_anno_ids = coco.getAnnIds(imgIds=val_imgs_id)

    # loop over the annotations to separate the train and validation images
    for j in range(num_anno):
        id_anno = data["annotations"][j]["id"]

        if id_anno in val_anno_ids:
            val_data["annotations"].append(data["annotations"][j])
        else:
            train_data["annotations"].append(data["annotations"][j])

    name_train_file = save_dir + '/train_' + path_json.split('/')[-1]
    name_val_file = save_dir + '/val_' + path_json.split('/')[-1]

    with open(name_train_file, 'w') as json_file:
        json.dump(train_data, json_file)

    with open(name_val_file, 'w') as json_file:
        json.dump(val_data, json_file)
