import cv2
import copy
import numpy as np
from typing import *
from tqdm import tqdm
from APEUS_data.EUS_video.Annotation.indexity_annotation import IndexAnnotation
from APEUS_data.EUS_video.utils import labels_correlation, bbox_rel2abs


def generate_bbox_colors(class_names: List[str]) -> Dict[str, Any]:
    """

    :param class_names:
    :return:
    """
    nb_colors = len(set(class_names))
    colors = np.random.randint(0, 255, [nb_colors, 3])
    colors_dict = {}

    for i, name in enumerate(list(set(class_names))):
        colors_dict[name] = list(colors[i])

    return colors_dict


class Visualize(object):

    def __init__(self, image: Optional[np.array] = None, video: Optional[cv2.VideoCapture] = None):
        """ Initialize a instance for visualization

        :param image: a image
        :param video: a video
        """
        self.image = image
        self.video = video

        if video:
            self.width = int(self.video.get(3))
            self.height = int(self.video.get(4))
            self.num_frames = int(self.video.get(cv2.CAP_PROP_FRAME_COUNT))
            self.fps = self.video.get(cv2.CAP_PROP_FPS)

    def _draw_bbox(self,
                   bbox: List[int],
                   class_name: str,
                   score: Optional[float] = None,
                   bbox_color: List[int] = [255, 0, 0],
                   text_color: List[int] = [255, 255, 255],
                   thickness: int = 2,
                   bbox_format: Optional[str] = None,
                   font_scale: float = 0.35) -> None:
        """ draw a bounding box with the label. Also, if a prediction score is provided, it will be displayed as well

        :param bbox: list of the bounding box coordinates
        :param class_name: the class name corresponding to the bounding box
        :param score: score corresponding to the bounding box
        :param bbox_color: list corresponding to the RBG value
        :param text_color: list of the RGB values of the text color
        :param thickness: thickness of the font
        :param bbox_format: if 'coco' the bbox format is supposed to be [x, y, w, h] otherwise [x1, y1, x2, y2]
        :param font_scale: value of the font scale
        """
        if bbox_format == 'coco':
            x_min, y_min, w, h = bbox
            x_min, x_max, y_min, y_max = int(x_min), int(x_min + w), int(y_min), int(y_min + h)
        else:
            x_min, y_min, x_max, y_max = map(int, bbox)
        # draw the bounding on the image
        r, b, g = map(int, bbox_color)  # make sure that the list of color value are <int>
        bbox_color = [r, b, g]
        cv2.rectangle(self.image, (x_min, y_min), (x_max, y_max), color=bbox_color, thickness=thickness)

        if score:
            label = class_name + ":  " + str(int(score * 100)) + "%"
        else:
            label = class_name

        # get the pixel dimension of the text
        ((text_width, text_height), _) = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, font_scale, 1)
        # draw a box on top the bounding box to highlight the text
        cv2.rectangle(self.image, (x_min, y_max - int(1.3 * text_height)), (x_min + text_width, y_max), bbox_color, -1)
        # draw the text
        cv2.putText(self.image,
                    text=label,
                    org=(x_min, y_max - int(0.3 * text_height)),
                    fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                    fontScale=font_scale,
                    color=text_color,
                    lineType=cv2.LINE_AA)

    def draw_image_annotations(self,
                               boxes: List[list],
                               class_names: List[str],
                               scores: Optional[List[float]] = None,
                               dict_colors: Optional[Dict[str, List[int]]] = None,
                               text_color: Optional[List[int]] = [255, 255, 255],
                               thickness: Optional[int] = 2,
                               bbox_format: Optional[str] = None,
                               font_scale: float = 0.35) -> None:
        """ draw annotations on a image. This method uses _draw_bbox method

        :param boxes: list of the bounding boxes
        :param class_names: list of the classes names corresponding to each bounding boxes
        :param scores: list of the scores corresponding to each bounding boxes
        :param dict_colors: dictionary where each class_name has a corresponding color list [R, B, G].
                            If None, a dictionary will be automatically generated and the color will be random
        :param text_color: list of the RGB values of the text color
        :param thickness: thickness of the font
        :param bbox_format: if 'coco' the bbox format is supposed to be [x, y, w, h] otherwise [x1, y1, x2, y2]
        :param font_scale: value of the font scale

        Example:

        .. code-block:: python

            image = np.zeros((500, 500, 3), np.uint8)
            image = Vizualize(image)
            image.draw_image_annotations(boxes=[[20, 100, 400, 200], [80, 150, 200, 350]], class_names=['chat', 'chien'],
             scores=[0.9145, 0.789454])
            image.display()

        """
        # if bbox colors is provided, we will use these colors otherwise, we will generate random one
        if dict_colors is None:
            dict_colors = generate_bbox_colors(class_names)

        if scores is not None:
            for bbox, class_name, score in zip(boxes, class_names, scores):
                self._draw_bbox(bbox=bbox,
                                class_name=class_name,
                                score=score,
                                bbox_color=dict_colors[class_name],
                                text_color=text_color,
                                thickness=thickness,
                                bbox_format=bbox_format,
                                font_scale=font_scale)
        else:
            for bbox, class_name in zip(boxes, class_names):
                self._draw_bbox(bbox=bbox,
                                class_name=class_name,
                                bbox_color=dict_colors[class_name],
                                text_color=text_color,
                                thickness=thickness,
                                bbox_format=bbox_format,
                                font_scale=font_scale)

    def draw_video_annotations(self,
                               path_new_video: str,
                               boxes: List[list],
                               class_names: Iterable[List[str]],
                               scores: Optional[Iterable[List[float]]] = None,
                               dict_colors: Dict[str, List[int]] = None,
                               text_color: Optional[List[int]] = [255, 255, 255],
                               thickness: Optional[int] = 2,
                               bbox_format: Optional[str] = None,
                               font_scale: float = 0.35) -> None:
        """ Draw the annotations on a video

        :param path_new_video: path where the new video will be saved
        :param boxes: table of bounding boxes where each row must correspond to the bounding boxes of a frame
        :param class_names: table of labels where each row must correspond to the labels of a frame
        :param scores: table of scores predictions where each row correspond to the scores
        :param dict_colors: dictionary where each class_name has a corresponding color list [R, B, G].
        :param text_color: list of the RGB values of the text color
        :param thickness: thickness of the font
        :param bbox_format: if 'coco' the bbox format is supposed to be [x, y, w, h] otherwise [x1, y1, x2, y2]
        :param font_scale: value of the font scale

        .. code-block:: python

            path_video = "/home/afleurentin/Bureau/000034_VID004_cut_0.mp4"
            vid = cv2.VideoCapture(path_video)
            video = Vizualize(video=vid)
            number_frames = video.num_frames
            # generate bounding boxes
            bboxes = np.ones((number_frames, 2, 4))
            bboxes[:, 0] = [20, 100, 400, 200]
            bboxes[:, 1] = [80, 150, 200, 350]
            # generate labels
            labels = [['chat', 'chien'] for k in range(number_frames)]
            # colors
            dict_colors = {'chat': [255, 0, 0], 'chien': [0, 255, 0]}
            # draw annotations
            video.draw_video_annotations(path_new_video="/home/afleurentin/Bureau/000034_VID004_cut_1.mp4",
                                         boxes=bboxes,
                                         class_names=labels,
                                         dict_colors=dict_colors)

        """

        print("[INFO] Drawing annotations...")
        # Create new video
        new_video = cv2.VideoWriter(filename=path_new_video,
                                    fourcc=cv2.VideoWriter_fourcc(*'mp4v'),
                                    fps=round(self.fps),
                                    frameSize=(self.width, self.height),
                                    apiPreference=0)

        progress_bar = tqdm(total=len(boxes))

        for index_frame in range(len(boxes)):
            progress_bar.update()
            ret, frame = self.video.read()

            # handle video read failure as end of video
            if not ret:
                return

            self.image = frame
            # get the bounding boxes, labels, scores for this frame
            frame_boxes = boxes[index_frame]
            frame_labels = class_names[index_frame]
            frame_scores = scores[index_frame] if scores else None

            if len(frame_boxes) != 0:
                self.draw_image_annotations(boxes=frame_boxes,
                                            class_names=frame_labels,
                                            scores=frame_scores,
                                            dict_colors=dict_colors,
                                            text_color=text_color,
                                            thickness=thickness,
                                            bbox_format=bbox_format,
                                            font_scale=font_scale)

            # save the new frame
            new_video.write(self.image)

        progress_bar.close()
        new_video.release()

    def mirror_predictions_ground_truth_apeus(self,
                                              path_gt: str,
                                              correlation_label: dict,
                                              path_new_video: str,
                                              boxes: List[list],
                                              class_names: Iterable[List[str]],
                                              scores: Optional[Iterable[List[float]]] = None,
                                              dict_colors: Dict[str, List[int]] = None,
                                              text_color: Optional[List[int]] = [255, 255, 255],
                                              thickness: Optional[int] = 2,
                                              bbox_format: Optional[str] = None,
                                              font_scale: float = 0.35) -> None:
        """ Create a video with a duplicate view of the original video. On the left side, we draw the predictions and
        on the right side, we draw the ground truth annotations

        :param path_gt: path to the annotation file of MosAIc
        :param correlation_label: dictionary containing the corresponding label for the ground truth
        :param path_new_video: path where the new video will be saved
        :param boxes: table of bounding boxes where each row must correspond to the bounding boxes of a frame
        :param class_names: table of labels where each row must correspond to the labels of a frame
        :param scores: table of scores predictions where each row correspond to the scores
        :param dict_colors: dictionary where each class_name has a corresponding color list [R, B, G].
        :param text_color: list of the RGB values of the text color
        :param thickness: thickness of the font
        :param bbox_format: if 'coco' the bbox format is supposed to be [x, y, w, h] otherwise [x1, y1, x2, y2]
        :param font_scale: value of the font scale

        """

        # get MosAIc annotations
        ground_truth = IndexAnnotation(path_json=path_gt)
        ground_truth.keep_only_type(type="structure")

        # Create new video
        new_video = cv2.VideoWriter(filename=path_new_video,
                                    fourcc=cv2.VideoWriter_fourcc(*'mp4v'),
                                    fps=round(self.fps),
                                    frameSize=(self.width * 2, self.height),
                                    apiPreference=0)

        progress_bar = tqdm(total=self.num_frames)

        for index_frame in range(self.num_frames):

            progress_bar.update()
            ret, frame = self.video.read()

            # handle video read failure as end of video
            if not ret:
                return

            # Prediction side
            self.image = copy.deepcopy(frame)
            cv2.putText(self.image,
                        text="Predictions",
                        org=(5, 30),
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=font_scale*1.5,
                        color=text_color,
                        lineType=cv2.LINE_AA)
            # get the predictions bounding boxes, labels, scores for this frame
            frame_boxes = boxes[index_frame]
            frame_labels = class_names[index_frame]
            frame_scores = scores[index_frame] if scores else None

            self.draw_image_annotations(boxes=frame_boxes,
                                        class_names=frame_labels,
                                        scores=frame_scores,
                                        dict_colors=dict_colors,
                                        text_color=text_color,
                                        thickness=thickness,
                                        bbox_format=bbox_format,
                                        font_scale=font_scale)

            prediction_frame = self.image
            # Ground truth side
            self.image = copy.deepcopy(frame)
            cv2.putText(self.image,
                        text="Ground Truth",
                        org=(5, 30),
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=font_scale*2,
                        color=text_color,
                        lineType=cv2.LINE_AA)

            # get the ground truth bounding boxes, labels for this frame
            timestamp_millisec = int((index_frame / self.fps) * 1000)
            labels = ground_truth.get_label(timestamp_millisec)
            bboxes = ground_truth.get_bbox(timestamp_millisec)

            if len(labels) != 0:
                # Select only the wanted labels with
                frame_labels, frame_boxes = labels_correlation(correlation_labels=correlation_label,
                                                               labels=labels,
                                                               bboxes=bboxes)
                if len(frame_boxes) != 0:

                    frame_boxes = bbox_rel2abs(bboxs=frame_boxes, width=self.width, height=self.height)

                    self.draw_image_annotations(boxes=frame_boxes,
                                                class_names=frame_labels,
                                                dict_colors=dict_colors,
                                                text_color=text_color,
                                                thickness=thickness,
                                                bbox_format="coco",
                                                font_scale=font_scale)

            # save the new frame
            final_frame = np.concatenate((prediction_frame, self.image), axis=1)
            new_video.write(final_frame)

        progress_bar.close()
        new_video.release()

    def display(self):

        cv2.imshow('mon image', self.image)
        cv2.waitKey(0)
