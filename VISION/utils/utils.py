import numpy as np
import torch
import argparse
import yaml
import logging
import cv2
from typing import Tuple


def synchronize_pycocotools_metrics(tensor: torch.Tensor = None, is_distributed: bool = False) -> torch.Tensor:
    """ Synchronize the metrics stats from pycocotools between the different processes.
    N = number of processes
    C = number of categories
    :param tensor: metrics stats from pycocotools dimensions = [N, 12] or [N, 12, C]
    :param is_distributed: True if more than one gpu is used, otherwise False
    :return: a tensor with an average value between process
    """
    # If more than one gpu is used
    if is_distributed:
        # For average metric
        if len(tensor.size()) == 2:
            # Initialize the average tensor
            average = torch.ones(12) * -1
            # Transpose the tensor dimension = [12, num_process]
            tensor = tensor.transpose(0, 1)
            # get the index where metrics value is -1
            minus_one_index = torch.eq(tensor, -1)

            for k in range(12):
                stats = tensor[k, ~minus_one_index[k]]
                if len(stats) != 0:
                    average[k] = torch.mean(stats)

            return average

        # For metric per categories
        else:
            # Initialize the category average tensor
            num_process, _, num_categories = tensor.size()
            category_average = torch.ones((12, num_categories)) * -1
            # transform the result tensor from [N, 12, C] to [12, C, N]
            tensor = torch.hstack(
                [torch.hstack([tensor[k][:, i].unsqueeze(0).transpose(0, 1) for k in range(num_process)])
                 for i in range(num_categories)]).view(12, num_categories, num_process)
            # get the index where metrics value is -1
            minus_one_index = torch.eq(tensor, -1)

            for k in range(12):
                for c in range(num_categories):
                    stats = tensor[k, c, ~minus_one_index[k, c]]
                    if len(stats) != 0:
                        category_average[k, c] = torch.mean(stats)

            return category_average

    # if only one gpu is used
    else:
        return tensor


def convert_coco_bboxes_to_tensor(bboxes: np.array) -> torch.Tensor:
    """ transform bounding boxes from [x, y, w, h] to [x1, y1, x2, y2]

    :param bboxes: bounding boxes
    :return: the bounding boxes in the format [x1, y1, x2, y2]
    """
    if len(bboxes.shape) == 1:
        bboxes[2] = bboxes[0] + bboxes[2]
        bboxes[3] = bboxes[1] + bboxes[3]

        return torch.from_numpy(bboxes)

    bboxes[:, 2] = bboxes[:, 0] + bboxes[:, 2]
    bboxes[:, 3] = bboxes[:, 1] + bboxes[:, 3]

    return torch.from_numpy(bboxes)


def convert_to_xywh(bboxes: torch.Tensor) -> torch.Tensor:
    """ transform bounding boxes from [x1, y1, x2, y2] to [x, y, w, h]

    :param bboxes: a pytorch tensor of bounding boxes
    :return: the converted tensor
    """
    xmin, ymin, xmax, ymax = bboxes.unbind(1)
    return torch.stack((xmin, ymin, xmax - xmin, ymax - ymin), dim=1)


def get_args(path_config: str) -> argparse.Namespace:
    """ Load a YAML file for the configuration of the model and return a argparse object usable by the LightningModule

    :param path_config: path to the config file (YAML)
    :return: return a argparse Namespace
    """

    args = yaml.load(open(path_config), Loader=yaml.FullLoader)

    parser = argparse.ArgumentParser(add_help=False)
    for name_param, value in args.items():
        parser.add_argument(f'--{name_param}', default=value)
    args = parser.parse_args()

    return args


class Params:
    " Load the parameters from a YAML file and create a dictionary"

    def __init__(self, config_file):
        self.params = yaml.load(open(config_file).read(), Loader=yaml.FullLoader)

    def __getattr__(self, item):
        return self.params.get(item, None)


def my_logger():
    """ Define a logger """
    logging.basicConfig(format='%(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
                        datefmt=None,
                        level=logging.INFO)

    return logging.getLogger(__name__)


def eus_layout_crop(image: cv2.IMREAD_COLOR) -> Tuple[np.array, slice, slice]:
    """ remove the ultrasound system layout from a eus image """
    # To simplify the identification between the ultrasound system
    _, thres = cv2.threshold(image, 160, 255, cv2.THRESH_BINARY)
    # test if it's a pentax ultrasound system
    if np.equal(thres[1000:1010, 640:660, 0], np.ones((10, 20)) * 255).all():
        slice_height = slice(128, 970)
        slice_width = slice(580, 1560)
    # otherwise it's a Hitachi ultrasound system
    else:
        slice_height = slice(120, 890)
        slice_width = slice(380, 1612)

    return image[slice_height, slice_width, :], slice_height, slice_width

