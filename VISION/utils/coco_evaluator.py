from VISION.utils.pycocotools.cocoeval import COCOeval
from VISION.utils.pycocotools.coco import COCO
from VISION.utils.utils import convert_to_xywh
from VISION.data.loaders.coco_loader import DetectDataModule
from collections import defaultdict


class CocoEvaluator(object):
    """ Compute the mAP """

    def __init__(self, coco_gt: DetectDataModule, iou_type: str = "bbox"):
        """
        :param coco_gt: detect module loader
        :param iou_types: type of intersection over union

        .. warning::
            For now, only iou_type "bbox" has been tested

        """
        # Create a dictionary for the ground truth annotations
        self.coco_gt = {'images': [], 'annotations': [], 'categories': []}
        self.coco_gt['categories'].extend([v for k, v in coco_gt.dataset_val.coco.cats.items()])
        # Create a dictionary for the predictions
        self.coco_pred = {'images': [], 'annotations': [], 'categories': []}
        self.coco_pred['categories'].extend([v for k, v in coco_gt.dataset_val.coco.cats.items()])
        self.iou_type = iou_type

        # Creation of a COCO object for both ground truth and prediction
        self.CocoGt = COCO()
        self.CocoDt = COCO()

        self._index_images_gt = 0
        self._index_annotations_gt = 0
        self._index_images_dt = 0
        self._index_annotations_dt = 0

        self.eval_imgs = []
        self.img_ids = []

    def update_gt(self, bboxes: list, labels: list) -> None:
        """ Update the information of the ground truth data

        :param bboxes: bounding boxes for each annotations inside the batch
        :param labels: labels for each annotations inside the batch
        """
        # Loop over the bounding boxes and labels in the data batch
        for image_boxes, image_labels in zip(bboxes, labels):
            self.coco_gt['images'].append({'id': int(self._index_images_gt)})

            # Loop over each bounding box and label
            for box, label in zip(image_boxes, image_labels):
                # box is a tuple, so we need to convert it into a list
                box = list(box)
                self.coco_gt['annotations'].append({'id': int(self._index_annotations_gt),
                                                    'category_id': int(label),
                                                    'image_id': int(self._index_images_gt),
                                                    'area': int(box[2] * box[3]),
                                                    'bbox': [int(coord) for coord in box],
                                                    'iscrowd': 0})
                # update the annotation index
                self._index_annotations_gt += 1

            # update the image index
            self._index_images_gt += 1

    def update_pred(self, predictions) -> None:
        """ update information for each predictions

        :param predictions:
        """
        for prediction in predictions:
            if len(prediction) == 0:
                continue
            self.coco_pred['images'].append({'id': int(self._index_images_dt)})

            boxes = prediction["boxes"]
            boxes = convert_to_xywh(boxes).tolist()
            scores = prediction["scores"].tolist()
            labels = prediction["labels"].tolist()

            for box, label, score in zip(boxes, labels, scores):
                self.coco_pred['annotations'].append({'id': int(self._index_annotations_dt),
                                                      'category_id': int(label),
                                                      'image_id': int(self._index_images_dt),
                                                      'area': int(box[2] * box[3]),
                                                      'bbox': [int(coord) for coord in box],
                                                      'score': score})

                # update the prediction index
                self._index_annotations_dt += 1
            # update the image index
            self._index_images_dt += 1

    def compute_metrics(self):
        """ Compute the mAP between the ground truth and the predictions

        :return:
        """
        # Create a usable ground truth COCO object
        self.CocoGt.dataset = self.coco_gt
        createIndex(self.CocoGt)
        # Create a usable prediction COCO object
        self.CocoDt.dataset = self.coco_pred
        createIndex(self.CocoDt)
        # Create the COCOeval
        coco_evaluator = COCOeval(self.CocoGt, self.CocoDt, iouType=self.iou_type)
        # Compute mAP
        coco_evaluator.evaluate()
        coco_evaluator.accumulate()
        coco_evaluator.summarize()
        coco_evaluator.summarize_per_category()

        return coco_evaluator


def createIndex(self: COCO) -> None:
    """ Take a COCO object which has only its dataset attribute define. Then it will define all the necessary attributes
    Thus we will get a fully usable COCO object

    :param self: a COCO object
    """

    anns, cats, imgs = {}, {}, {}
    img_to_anns, cat_to_imgs = defaultdict(list), defaultdict(list)
    if 'annotations' in self.dataset:
        for ann in self.dataset['annotations']:
            img_to_anns[ann['image_id']].append(ann)
            anns[ann['id']] = ann

    if 'images' in self.dataset:
        for img in self.dataset['images']:
            imgs[img['id']] = img

    if 'categories' in self.dataset:
        for cat in self.dataset['categories']:
            cats[cat['id']] = cat

    if 'annotations' in self.dataset and 'categories' in self.dataset:
        for ann in self.dataset['annotations']:
            cat_to_imgs[ann['category_id']].append(ann['image_id'])

    # create class members
    self.anns = anns
    self.imgToAnns = img_to_anns
    self.catToImgs = cat_to_imgs
    self.imgs = imgs
    self.cats = cats


name_metrics = {
    0: "Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ]",
    1: "Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ]",
    2: "Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ]",
    3: "Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ]",
    4: "Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ]",
    5: "Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ]",
    6: "Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ]",
    7: "Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ]",
    8: "Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ]",
    9: "Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ]",
    10: "Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ]",
    11: "Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ]",
}