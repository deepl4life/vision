import torch
import numpy as np
import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset, Sampler
from torchvision import transforms
from typing import List, Optional, Callable
from math import ceil
import random

from VISION.models.detection.my_transforms import Rescale_0_1
from VISION.utils.utils import convert_coco_bboxes_to_tensor


def _collate_fn(batch: List[torch.Tensor]) -> tuple:
    return tuple(zip(*batch))


class DetectDataModule(LightningDataModule):
    """ Data module to load the data for training, validation and prediction pipelines.

    To use this module, you need:

    - a directory containing the images for training and validation (both in the same directory)
    - a json file containing the training annotations (COCO format)
    - a json file containing the validation annotations (COCO format)
    """

    def __init__(self,
                 data_dir: str = None,
                 path_json_train: str = None,
                 path_json_val: str = None,
                 train_dataset: Callable = None,
                 val_dataset: Callable = None,
                 num_workers: int = 0,
                 normalize: bool = False,
                 resize: tuple = (600, 600),
                 batch_size: int = 2,
                 pin_memory: bool = False,
                 drop_last: bool = False,
                 subfolder: bool = False
                 ) -> None:
        """

        :param str data_dir: path to where the data are saved
        :param str path_json_train: path to the training annotation file (in COCO format)
        :param str path_json_val: path to the validation annotation file (in COCO format)
        :param int num_workers: how many workers to use for loading data
        :param bool normalize: if true applies image normalization
        :param tuple resize: set the size at which the images will be resize
        :param int batch_size: how many samples per batch to load
        :param bool pin_memory: if true, the data loader will copy Tensors into CUDA pinned memory before returning them
        :param bool drop_last: if true, drops the last incomplete batch
        """

        super().__init__()
        self.data_dir = data_dir
        self.path_json_train = path_json_train
        self.path_json_val = path_json_val

        self.train_dataset = train_dataset
        self.val_dataset = val_dataset

        self.num_workers = num_workers
        self.normalize = normalize
        self.resize = resize
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.drop_last = drop_last
        self.subfolder = subfolder

    def setup(self, stage: Optional[str] = None):
        """
        Creates train and val dataset
        """
        if stage == "fit" or stage is None:
            self.dataset_train = self.train_dataset(path_images_dir=self.data_dir,
                                                    path_json=self.path_json_train,
                                                    transform=self.train_transforms,
                                                    subfolder=self.subfolder)

            self.dataset_val = self.val_dataset(path_images_dir=self.data_dir,
                                                path_json=self.path_json_val,
                                                transform=self.val_transforms,
                                                subfolder=self.subfolder)
            self.num_classes = self.dataset_val.num_classes()
            self.labels = self.dataset_val.labels.values()

    @property
    def train_transforms(self):
        if self.normalize:
            return A.Compose([
                A.HorizontalFlip(p=0.25),
                A.Resize(self.resize[0], self.resize[1]),
                A.Normalize(mean=(0.559, 0.574, 0.566), std=(0.393, 0.395, 0.399), max_pixel_value=1.0),
                ToTensorV2(),
                Rescale_0_1()],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))
        else:
            return A.Compose([

                A.Resize(self.resize[0], self.resize[1]),
                A.HorizontalFlip(p=0.25),
                A.VerticalFlip(p=0.25),
                A.RandomRotate90(p=0.5),
                # A.CLAHE(p=0.5),
                # A.ColorJitter(p=0.25),
                # A.IAAEmboss(p=0.25),
                ToTensorV2(),
                Rescale_0_1()
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))

    @property
    def val_transforms(self):
        if self.normalize:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                ToTensorV2(),
                A.Normalize(mean=(0.559, 0.574, 0.566), std=(0.393, 0.395, 0.399), max_pixel_value=1.0),
                Rescale_0_1()],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))
        else:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                ToTensorV2(),
                Rescale_0_1()
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))

    def train_dataloader(self) -> DataLoader:
        """ The Train dataloader """
        return self._data_loader(self.dataset_train, shuffle=True)

    def val_dataloader(self) -> DataLoader:
        """ The validation dataloader """
        return self._data_loader(self.dataset_val, shuffle=False)

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """

        if self.subfolder:
            l_imgs_per_patients = dataset.get_ids_per_patient()
            patient_sampler = PatientSampler(
                l_imgs_per_patients, batch_size=self.batch_size, drop_last=self.drop_last)

            dataLoader = DataLoader(dataset=dataset,
                                    batch_size=self.batch_size,
                                    # shuffle=shuffle,
                                    num_workers=self.num_workers,
                                    drop_last=self.drop_last,
                                    pin_memory=self.pin_memory,
                                    sampler=patient_sampler,
                                    collate_fn=_collate_fn)
        else:
            dataLoader = DataLoader(dataset=dataset,
                                    batch_size=self.batch_size,
                                    shuffle=shuffle,
                                    num_workers=self.num_workers,
                                    drop_last=self.drop_last,
                                    pin_memory=self.pin_memory,
                                    collate_fn=_collate_fn)

        return dataLoader


class PatientSampler(Sampler):
    def __init__(self, l_imgs_per_patients, batch_size=2,
                 drop_last=True, n_samples_per_patients=50):
        self.l_imgs_per_patients = l_imgs_per_patients
        self.n_samples_per_patients = n_samples_per_patients
        self.batch_size = batch_size
        self.drop_last = drop_last

    def __iter__(self):
        """return batch of index
        index are draw from a subset of the indexs, so that it contains a fixed number
        of images per patient, to ensure balance between lesions.

        Yields:
            [type]: [description]
        """
        l_idx_current_epoch = []

        for ixd_imgs_patient in self.l_imgs_per_patients:
            # select a random subset of image of the desired size.
            selected_ixd = random.sample(ixd_imgs_patient, self.n_samples_per_patients)
            l_idx_current_epoch.extend(selected_ixd)

        random.shuffle(l_idx_current_epoch)

        # split ids in batches        
        batchs = [l_idx_current_epoch[x:x + self.batch_size]
                  for x in range(0, len(l_idx_current_epoch), self.batch_size)]

        if self.drop_last and (len(batchs[-1]) < self.batch_size):
            batchs = batchs[:-1]

        for batch in batchs:
            yield from batch

    def __len__(self):
        n_imgs = len(self.l_imgs_per_patients) * self.n_samples_per_patients
        res = ceil(n_imgs / 1)  # self.batch_size)
        return res
