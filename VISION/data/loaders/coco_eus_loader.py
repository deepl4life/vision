import torch
import numpy as np
import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2
from typing import List, Optional
from torch.utils.data import DataLoader, Dataset
from VISION.models.detection.my_transforms import Rescale_0_1
from VISION.data.loaders.coco_loader import DetectDataModule
from VISION.utils.utils import convert_coco_bboxes_to_tensor


def _collate_fn(batch: List[torch.Tensor]) -> tuple:
    return tuple(zip(*batch))


def _apeus_coco_collate_fn(batch: List[torch.Tensor]):
    """ collate_fn specific for APEUS coco dataset """
    images, bboxes, labels, patient_ids = zip(*batch)

    # Transform what is returned by the dataloader into a correct input for the model
    targets = []
    # Loop over the images inside the batch
    for box, label in zip(bboxes, labels):

        if len(label) == 0:
            target_dict = {"boxes": torch.zeros((0, 4), dtype=torch.float32),
                           "labels": torch.zeros(1, dtype=torch.int64)}
        else:
            target_dict = {"boxes": convert_coco_bboxes_to_tensor(np.array(box)),
                           "labels": torch.from_numpy(np.array(label)).type(dtype=torch.int64)}
        targets.append(target_dict)

    return images, targets, patient_ids


class DetectEusDataModule(DetectDataModule):
    """ LightDataModule to handle the APEUS data"""

    def setup(self, stage: Optional[str] = None):
        """
        Creates train and val dataset
        """
        if stage == "fit" or stage is None:
            self.dataset_train = self.train_dataset(path_images_dir=self.data_dir,
                                                    path_json=self.path_json_train,
                                                    transform=self.train_transforms,
                                                    subfolder=False)

            self.dataset_val = self.val_dataset(path_images_dir=self.data_dir,
                                                path_json=self.path_json_val,
                                                transform=self.val_transforms,
                                                subfolder=False)
            self.num_classes = self.dataset_val.num_classes()
            self.labels = self.dataset_val.labels.values()

            self.num_patients = self.dataset_val.num_patients()
            self.patient_ids = self.dataset_val.patient_ids()

        if stage == "predict":
            self.dataset_predict = self.predict_dataset(path_video=self.path_video,
                                                        transform=self.predict_transforms)

    @property
    def train_transforms(self):
        if self.normalize:
            return A.Compose([
                A.HorizontalFlip(p=0.25),
                A.Resize(self.resize[0], self.resize[1]),
                A.Normalize(mean=(0.559, 0.574, 0.566), std=(0.393, 0.395, 0.399), max_pixel_value=1.0),
                ToTensorV2(),
                Rescale_0_1()],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids', 'patient_id']))
        else:
            return A.Compose([

                A.Resize(self.resize[0], self.resize[1]),
                A.HorizontalFlip(p=0.25),
                A.VerticalFlip(p=0.25),
                A.RandomRotate90(p=0.5),
                # A.CLAHE(p=0.5),
                A.ColorJitter(p=0.25),
                A.IAAEmboss(p=0.25),
                ToTensorV2(),
                Rescale_0_1()
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids', 'patient_id']))

    @property
    def val_transforms(self):
        if self.normalize:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                ToTensorV2(),
                A.Normalize(mean=(0.559, 0.574, 0.566), std=(0.393, 0.395, 0.399), max_pixel_value=1.0),
                Rescale_0_1()],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids', 'patient_id']))
        else:
            return A.Compose([
                A.Resize(self.resize[0], self.resize[1]),
                ToTensorV2(),
                Rescale_0_1()
            ],
                bbox_params=A.BboxParams(format='coco', label_fields=['category_ids', 'patient_id']))

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=self.drop_last,
                          pin_memory=self.pin_memory,
                          collate_fn=_collate_fn)