import torch
import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2
from pytorch_lightning import LightningDataModule
from torch.utils.data import DataLoader, Dataset
from typing import List, Optional

from VISION.data.dataset.coco_dataset_classif import CocoDataset


def _collate_fn(batch: List[torch.Tensor]) -> tuple:
    return tuple(zip(*batch))


class DetectEusDataModule(LightningDataModule):
    """ Data module to load the data for training pipeline.

    To use this module, you need:

    - a directory containing the images for training and validation (both in the same directory)
    - a json file containing the training annotations (COCO format)
    - a json file containing the validation annotations (COCO format)
    """

    def __init__(self,
                 data_dir: str = None,
                 path_json_train: str = None,
                 path_json_val: str = None,
                 num_workers: int = 0,
                 normalize: bool = False,
                 resize: tuple = (600, 600),
                 batch_size: int = 2,
                 pin_memory: bool = False,
                 drop_last: bool = False,
                 ) -> None:
        """

        :param str data_dir: path to where the data are saved
        :param str path_json_train: path to the training annotation file (in COCO format)
        :param str path_json_val: path to the validation annotation file (in COCO format)
        :param int num_workers: how many workers to use for loading data
        :param bool normalize: if true applies image normalization
        :param tuple resize: set the size at which the images will be resize
        :param int batch_size: how many samples per batch to load
        :param bool pin_memory: if true, the data loader will copy Tensors into CUDA pinned memory before returning them
        :param bool drop_last: if true, drops the last incomplete batch
        """

        super().__init__()
        self.data_dir = data_dir
        self.path_json_train = path_json_train
        self.path_json_val = path_json_val
        self.num_workers = num_workers
        self.normalize = normalize
        self.resize = resize
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.drop_last = drop_last

    def setup(self, stage: Optional[str] = None):
        """
        Creates train and val dataset
        """
        if stage == "fit" or stage is None:
            self.dataset_train = CocoDataset(path_images_dir=self.data_dir,
                                             path_json=self.path_json_train,
                                             transform=self.train_transforms)

            self.dataset_val = CocoDataset(path_images_dir=self.data_dir,
                                           path_json=self.path_json_val,
                                           transform=self.val_transforms)
        if stage == "test":
            self.dataset_test = CocoDataset(path_images_dir=self.data_dir,
                                            path_json=self.path_json_val,
                                            transform=self.val_transforms)

    @property
    def normalize_transform(self):
        return A.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

    @property
    def train_transforms(self):
        return A.Compose([
            A.HorizontalFlip(p=0.25),
            # A.Rotate(limit=30, p=0.25),
            A.Resize(self.resize[0], self.resize[1]),
            ToTensorV2()],
            bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))

    @property
    def val_transforms(self):

        return A.Compose([
            A.Resize(self.resize[0], self.resize[1]),
            ToTensorV2()],
            bbox_params=A.BboxParams(format='coco', label_fields=['category_ids']))

    def train_dataloader(self) -> DataLoader:
        """ The Train dataloader """
        return self._data_loader(self.dataset_train, shuffle=True)

    def val_dataloader(self) -> DataLoader:
        """ The validation dataloader """
        # return self._data_loader(self.dataset_val, shuffle=False)
        return DataLoader(dataset=self.dataset_val,
                    batch_size=1,
                    shuffle=False,
                    num_workers=self.num_workers,
                    drop_last=self.drop_last,
                    pin_memory=self.pin_memory,
                    collate_fn=_collate_fn)

    def test_dataloader(self) -> DataLoader:
        """ The validation dataloader """
        return self._data_loader(self.dataset_test, shuffle=False)

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=self.drop_last,
                          pin_memory=self.pin_memory,
                          collate_fn=_collate_fn)
