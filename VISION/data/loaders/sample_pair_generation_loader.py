import torch
from typing import List
from torch.utils.data import DataLoader, Dataset
from pytorch_lightning import LightningDataModule
from VISION.data.dataset.sample_pair_generation_dataset import SamplePairGeneration


class SimCLRDataModule(LightningDataModule):

    def __init__(self,
                 train_dataset: SamplePairGeneration,
                 val_dataset: SamplePairGeneration,
                 num_workers: int = 0,
                 batch_size: int = 2,
                 pin_memory: bool = True,
                 drop_last: bool = False):
        """

        :param train_dataset:
        :param val_dataset:
        :param num_workers:
        :param batch_size:
        :param pin_memory:
        :param drop_last:
        """
        super().__init__()
        self.train_dataset = train_dataset
        self.val_dataset = val_dataset
        self.num_workers = num_workers
        self.batch_size = batch_size
        self.pin_memory = pin_memory
        self.drop_last = drop_last

    @staticmethod
    def _collate_fn(batch: List[torch.Tensor]) -> tuple:
        return tuple(zip(*batch))

    def train_dataloader(self) -> DataLoader:
        """ The Train dataloader """
        return self._data_loader(self.train_dataset)

    def val_dataloader(self) -> DataLoader:
        """ The validation dataloader """
        return self._data_loader(self.val_dataset)

    def _data_loader(self, dataset: Dataset, shuffle: bool = False) -> DataLoader:
        """ Create DataLoader from Dataset

        :param torch.utils.data.Dataset dataset: the dataset
        :param bool shuffle: if true the dataset will be shuffle
        :return: a dataloader
        """
        return DataLoader(dataset=dataset,
                          batch_size=self.batch_size,
                          shuffle=shuffle,
                          num_workers=self.num_workers,
                          drop_last=self.drop_last,
                          pin_memory=self.pin_memory,
                          collate_fn=self._collate_fn)

