import cv2
import json
import random
import torchvision
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset
from VISION.utils.utils import eus_layout_crop
from VISION.models.self_supervised.simclr.transform import SimCLRDataTransform


class ApeusPairSampler:
    """ Sample a positive pair of images from a video.

    Detail of the process:
    - Select a random clip from a video
    - pick up 3 random images from this clip (img1, img2, img3).
    - img2 is consider as an anchor
    - Blend img1 and img2 together as well as img3 and img2
    - return the result of the blending

    """

    def __init__(self,
                 clip_duration: int = 10,
                 blending_factor: float = 0.5):
        """
        :param clip_duration: duration in seconds of the random clip which will be selected
        :param blending_factor: coefficient to blend a image and the anchor
        """
        self.clip_duration = clip_duration
        self.blending_factor = blending_factor

    def load_video(self, name_video):

        self.video = cv2.VideoCapture(name_video)
        # Check if the video file is usable
        if not self.video.isOpened():
            raise ValueError(f"Error reading video file : {name_video}")

        self.num_frames, self.fps, self.duration = self.get_metadata()

        if self.duration <= self.clip_duration:
            raise ValueError(f"The video {name_video} is too short or the clip duration is too long")

        _, frame = self.video.read()
        _, self.slice_height, self.slice_width = eus_layout_crop(frame)

    def get_metadata(self):
        num_frames = int(self.video.get(cv2.CAP_PROP_FRAME_COUNT))
        fps = self.video.get(cv2.CAP_PROP_FPS)
        # duration of the video in seconds
        duration = int(num_frames / fps)
        return num_frames, fps, duration

    def select_clip(self):
        # number of possible clips
        num_clip = self.duration // self.clip_duration
        index_clip = random.randint(0, num_clip - 1)
        index_start_frame = int(index_clip * self.clip_duration * self.fps)
        index_end_frame = int((index_clip + 1) * self.clip_duration * self.fps)
        return index_start_frame, index_end_frame

    def select_frames(self):
        index_start_frame, index_end_frame = self.select_clip()
        index_frames = list(np.random.choice(np.arange(index_start_frame, index_end_frame), size=3, replace=False))
        index_frames = sorted(index_frames)

        frames = []
        for id_frame in index_frames:
            self.video.set(cv2.CAP_PROP_POS_FRAMES, id_frame)
            _, frame = self.video.read()
            frame = frame[self.slice_height, self.slice_width, :]
            frames.append(frame)

        return frames

    def get_positive_pair(self):
        alpha = self.blending_factor
        beta = (1.0 - alpha)
        image_1, anchor, image_3 = self.select_frames()
        sample_1 = cv2.addWeighted(image_1, alpha, anchor, beta, 0.0)
        sample_2 = cv2.addWeighted(image_3, alpha, anchor, beta, 0.0)

        return sample_1, sample_2


class SamplePairGeneration(Dataset):
    """ Generate a positive pair for self-supervised training

    To work, this class required a specific format of json file.
    It must have the following format:
    {ID_PATIENT (int): [
             {
             INDEX_VIDEO_1_PROCEDURE_1 (int): name_video,
             INDEX_VIDEO_2_PROCEDURE_1 (int): name_video,
             },
             {
             INDEX_VIDEO_1_PROCEDURE_2 (int): name_video,
             INDEX_VIDEO_2_PROCEDURE_2 (int): name_video,
             },
        ],
    }

    """

    def __init__(self,
                 path_dir_video: str = None,
                 path_json: str = None,
                 transform: torchvision.transforms.Compose = None,
                 positive_pair_sampler: ApeusPairSampler = None,
                 extension_factor: int = 1):
        """

        :param path_dir_video: path of the videos repository
        :param path_json: path to the json file
        :param transform: data augmentation applied to the pair of images
        :param positive_pair_sampler: strategy to sample the positive pair
        :param extension_factor: As the length of our dataset corresponds to the number of
        patients, we can't do large batches (len(batch)>56). That's an issue because self-supervised methods require
        large batches to work properly. To solve this issue, we introduce a factor to artificially extend the number of
        patients inside the dataset. extension_factor >= 1.
        """

        self.path_dir_video = path_dir_video
        self.path_json = path_json
        self.transform = transform
        self.pair_sampler = positive_pair_sampler
        self.extension_factor = extension_factor
        self.data = self._load_data_from_json()
        self.id_patient = self.get_id_patient()

    def _load_data_from_json(self):
        with open(self.path_json) as data:
            return json.load(data)

    def get_id_patient(self):
        id_patient = list(self.data.keys())
        random.shuffle(id_patient)
        return id_patient * self.extension_factor

    def get_video(self, index_patient: int):
        procedures = self.data[self.id_patient[index_patient]]
        videos = procedures[random.randint(0, len(procedures) - 1)]
        name_video = videos[str(random.randint(1, len(videos)))]
        return self.path_dir_video + "/" + name_video

    def __len__(self):
        return len(self.id_patient)

    def __getitem__(self, index_patient: int):
        name_video = self.get_video(index_patient)
        self.pair_sampler.load_video(name_video)

        img1, img2 = self.pair_sampler.get_positive_pair()
        img1, img2 = self.transform(img1), self.transform(img2)
        return img1, img2
