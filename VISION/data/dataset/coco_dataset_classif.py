# import necessary packages
import os
import cv2
import torch
import numpy as np
import albumentations as A
from torch._C import dtype
from torch.utils.data import Dataset
from pycocotools.coco import COCO
from typing import Tuple
import random


class CocoDataset(Dataset):
    """
    Load images and a MS COCO json file link to the images, in order to be used
    by Pytorch
    """

    def __init__(self,
                 path_images_dir: str = None,
                 path_json: str = None,
                 transform: A.Compose = None):
        """ Initialize the CocoDataset

        :param str path_images_dir: path to the images directory
        :param str path_json: path to the corresponding json file
        :param transform: a list of Transform object
        """
        self.path_images_dir = path_images_dir
        self.path_json = path_json
        self.transform = transform
        self.coco = COCO(annotation_file=self.path_json)
        self.image_ids = self.coco.getImgIds()

        self.coco_labels = {}
        self.coco_labels_inverse = {}
        self.classes = {}
        self.labels = {}
        self.load_classes()

    def load_classes(self):
        """ Load the classes and create several dictionaries to save the information.

        Dictionaries configuration:
            coco_labels: {key: index_coco_label}
            coco_labels_inverse: {index_coco_label: key}
            classes: {label_name: key}
            labels: {key: label_name}

        Example:

        .. code-block:: python

            coco_labels: {0: 1, 1: 2}
            coco_labels_inverse: {1: 0, 2: 1}
            classes: {'paren_homo_hype': 0, 'duct_wirs_std': 1}
            labels: {0: 'paren_homo_hype', 1: 'duct_wirs_std'}

        """
        # Get categories:
        categories = self.coco.loadCats(self.coco.getCatIds())
        # Make sure the labels are in order
        categories.sort(key=lambda x: x['id'])

        for c in categories:
            self.coco_labels[len(self.classes)] = c['id']
            self.coco_labels_inverse[c['id']] = len(self.classes)
            self.classes[c['name']] = len(self.classes)

        for name_label, key in self.classes.items():
            self.labels[key] = name_label

    def coco_label_to_label(self, label):
        return self.coco_labels_inverse[label]

    def label_to_coco_label(self, label):
        return self.coco_labels[label]

    def load_image(self, image_index: int = None) -> np.ndarray:
        """ load an image and normalized it between 0 and 1.

        :param image_index:
        :return: the normalized ([0,1]) image corresponding to the index provided
        """
        # Get information on the image in order to get its name
        image_info = self.coco.loadImgs(self.image_ids[image_index])[0]
        # Get full path to the image
        path_image = os.path.join(self.path_images_dir, image_info['filename'])
        image = cv2.imread(path_image)
        # normalized the image
        image = image.astype(np.float32) / 255.0
        return image

    def image_aspect_ratio(self, image_index: int = None) -> float:
        """ Compute the aspect ratio of an image

        :param image_index:
        :return: the ratio of the width on the height
        """
        image = self.coco.loadImgs(self.image_ids[image_index])[0]

        return float(image['width']) / float(image['height'])

    def load_annotations(self, image_index: int = None) -> np.ndarray:
        """ Load the bounding boxes coordinates and labels for an image.

        :param image_index: the index of the image
        :return: a ndarray with the following size [number_annotations, 5]. It contains the bounding
                 boxes coordinates and labels id
        """
        # Get the annotations id from an image
        annotations_id = self.coco.getAnnIds(imgIds=self.image_ids[image_index], iscrowd=False)
        annotations = np.zeros((0, 5))

        # Some images might have no annotations, so we just check out this case
        if len(annotations_id) == 0:
            return annotations

        coco_annotations = self.coco.loadAnns(annotations_id)
        for idx, ann in enumerate(coco_annotations):

            # If an annotation have basically no width / height, we skip it
            if ann['bbox'][2] < 1 or ann['bbox'][3] < 1:
                continue

            annotation = np.zeros((1, 5))
            annotation[0, :4] = ann['bbox']
            annotation[0, 4] = ann['category_id']
            annotations = np.append(annotations, annotation, axis=0)

        # # transform from [x, y, w, h] to [x1, y1, x2, y2]
        # annotations[:, 2] = annotations[:, 0] + annotations[:, 2]
        # annotations[:, 3] = annotations[:, 1] + annotations[:, 3]
        # # sometimes [x, y, w, h] have wrong values, resulting some errors because [x1, y1, x2, y2] are then inconsistent
        # # so we need to check if [x1, y1, x2, y2] are correct
        # keep = (annotations[:, 3] > annotations[:, 1]) & (annotations[:, 2] > annotations[:, 0])
        # annotations = annotations[keep]

        return annotations

    def num_classes(self) -> int:
        """
        :return: the number of classes
        """
        return len(self.classes)

    def __len__(self):
        """
        :return: the number of images contains in the dataset
        """
        return len(self.image_ids)

    def __getitem__(self, idx: int = None) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor, torch.Tensor]:
        """ Gather an image and its annotations in a single dictionary

        :param idx: index of an image
        :return: a dict containing an image and the corresponding annotations
        """
        image = self.load_image(idx)
        target = self.load_annotations(idx)
        bboxes = target[:, :4]
        category_ids = target[:, 4]

        # Applied transformation
        transformed = self.transform(image=image, bboxes=bboxes, category_ids=category_ids)

        # Create masks from a random object       

        # create 0 mask images of same size
        mask_dim = transformed['image'].shape
        mask = np.zeros([1, mask_dim[1], mask_dim[2]])

        index_random = random.randrange(0, len(transformed['bboxes']))
        # choose a random box x y w h
        x, y, w, h = [int(x) for x in transformed['bboxes'][index_random]]

        # draw mask
        mask[0, y:y+h, x:x+w] = 1
        mask = torch.from_numpy(mask)

        # convert labels to one-hot for classification task
        label_mask = torch.nn.functional.one_hot(torch.tensor(transformed['category_ids'][index_random] - 1).to(torch.int64), num_classes=2) 
        
        transformed['image'] = transformed['image'].type(dtype=torch.float32)
        mask = mask.type(dtype=torch.float32)
        label_mask = label_mask.type(dtype=torch.float32)

        return transformed['image'], mask, label_mask
        # return transformed['image'], transformed['bboxes'], transformed['category_ids'], mask, [x, y, w, h]
